#!/usr/bin/env python3

import argparse
import os
import shutil
import subprocess
import sys
import platform


def parse_args():
    parser = argparse.ArgumentParser(description="Copy input to clipboard.")
    parser.add_argument(
        "-n",
        action="store_true",
        default=True,
        help="Strip newline from the end of input (default: True)",
    )
    parser.add_argument("-f", help="File to copy from instead of stdin")
    parser.add_argument(
        "-N",
        action="store_false",
        dest="n",
        help="Do not strip newline from the end of input",
    )
    parser.add_argument("-p", action="store_true", help="Use pbpaste")
    return parser.parse_args()


def main():
    args = parse_args()
    clip = None

    if "SSH_CONNECTION" in os.environ:
        if shutil.which("osc"):
            clip = ["osc", "copy"]
        elif shutil.which("osc-copy"):
            clip = ["osc-copy"]
    else:
        if shutil.which("pbcopy") and platform.system() == "Darwin":
            clip = ["pbcopy"]
        elif shutil.which("xclip"):
            clip = ["xclip", "-i", "-selection", "clipboard"]
        elif shutil.which("clip.exe"):
            clip = ["clip.exe"]
        elif os.environ.get("XDG_SESSION_TYPE") == "wayland":
            clip = ["wl-copy", "-n"] if args.p else ["wl-copy"]

    if not clip:
        print("no clip found")
        sys.exit(1)

    if not shutil.which(clip[0]):
        print("no clip found")
        sys.exit(1)

    if args.f:
        with open(args.f, "rb") as f:
            text = f.read()
            if args.n and text.endswith(b"\n"):
                text = text[:-1]
            process = subprocess.Popen(clip, stdin=subprocess.PIPE)
            process.communicate(input=text)
        sys.exit(0)

    input_text = sys.stdin.read()
    if args.n:
        input_text = input_text.rstrip("\n")

    process = subprocess.Popen(clip, stdin=subprocess.PIPE)
    process.communicate(input=input_text.encode())


if __name__ == "__main__":
    main()
