#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Chmouel Boudjnah <chmouel@chmouel.com>
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
#
# Harpon let you switch to a bunch of windows you define, and if it exist it
# will cycle them in order.
#
# This was tested on sway but this should work flawlessly on i3 too (but not
# tested)
#
# You simply pass the configuration as JSON to the argument of this script
#
# harpon.py '["firefox", "jetbrains-goland", "gnome-terminal"]'
#
# That configuration will switch to all those windows in order across all
# desktop that has this app_id or class_name (xwayland/i3)
#
# It supports the concept of alternatives, for example if you have multiple
# browser and you only fire up sometime google-chrome but mostly use firefox,
# then it will try to switch to google-chrome first and if it's not there it
# will try to use google-chrome first and if it exist then don't try to switch
# to firefox.
#
# [["google-chrome", "firefox"], ["code-url-handler", "jetbrains-goland"],
# "gnome-terminal"]
#
# if chrome is running it will use this one first and then try firefox otherwise
#
# If you have some configurations but none of them has been found it will exit 1
#
# TODO: Do something maybe when there is multiple windows of the same
# class/app_id maybe cycle through them. We pick only the first one for now
#
#

import json
import sys

import i3ipc

# The windows I want to jump to if exist
DEFAULT_CONFIG = [
    "firefox",
    ["code-url-handler", "emacs", "jetbrains-goland"],
    "kitty",
]


def find_and_focus(i3: i3ipc.Con, allfocuses: list) -> bool:
    for tofocus in allfocuses:
        # find al windows in workspace and see if an app_id match
        for ws in i3.get_workspaces():
            workspace = i3.get_tree().find_by_id(ws.ipc_data["id"])
            for w in workspace:
                app_id = w.ipc_data.get("app_id")
                if "window_properties" in w.ipc_data:
                    app_id = w.ipc_data["window_properties"]["class"]
                if app_id == tofocus:
                    w.command("focus")
                    return True
    return False


# take a list that can have list inside and find if the value exist
# if it does return the index in the list
def find_in_index_list(vlist: list, value: str) -> int:
    for i, v in enumerate(vlist):
        if isinstance(v, list):
            if value in v:
                return i
        else:
            if v == value:
                return i
    return -1


def harpon(windows_config: list) -> bool:
    i3 = i3ipc.Connection()
    tree = i3.get_tree()
    focused = tree.find_focused()
    app_id = focused.app_id
    if not app_id:
        app_id = focused.window_class

    # flatten windows variable
    flatten_windows = []
    for w in windows_config:
        if isinstance(w, list):
            flatten_windows.extend(w)
        else:
            flatten_windows.append(w)

    # get the next window
    if app_id not in flatten_windows:
        nindex = 0
    else:
        windows_index = find_in_index_list(windows_config, app_id)
        nindex = windows_index + 1
        if nindex >= len(windows_config):
            nindex = 0

    reordered = windows_config[nindex : len(windows_config)]
    reordered.extend(windows_config[0 : nindex - 1])

    jumped = False
    for w in reordered:
        if not isinstance(w, list):
            w = [w]
        if find_and_focus(i3, w):
            jumped = True
            break
    return jumped


if __name__ == "__main__":
    config = DEFAULT_CONFIG
    if len(sys.argv) > 1:
        config = json.loads(sys.argv[1])
    ret = harpon(config)
    if not ret:
        sys.exit(1)
