#!/usr/bin/env bash
# resizes the window to full height and 50% width and moves into the center of the secreen
# adapted from https://unix.stackexchange.com/a/53228 so we center the window by
# default unless we have the rotate argument and then we rotate the window
# around the screen from 1/3 => 2/3 => 3/3

set -x
#define the height in px of the top system-bar:
TOPMARGIN=27

#sum in px of all horizontal borders:
RIGHTMARGIN=10

# Enlarge Width when centering
ENLARGE_WIDTH=400

# get width of screen and height of screen
SCREEN_WIDTH=$(xwininfo -root | awk '$1=="Width:" {print $2}')
SCREEN_HEIGHT=$(xwininfo -root | awk '$1=="Height:" {print $2}')

PREVIOUS=$HOME/.cache/rotatewindow/cache
[[ -d $(dirname ${PREVIOUS}) ]] || mkdir -p $(dirname ${PREVIOUS})

[[ ${1} == "rotate" ]] && rotatearound=true

eval $(xwininfo -id $(xdotool getactivewindow) |
    sed -n -e "s/^ \+Absolute upper-left X: \+\([0-9]\+\).*/currentx=\1/p" \
           -e "s/^ \+Absolute upper-left Y: \+\([0-9]\+\).*/currenty=\1/p" \
           -e "s/^ \+Width: \+\([0-9]\+\).*/currentw=\1/p" \
           -e "s/^ \+Height: \+\([0-9]\+\).*/currenth=\1/p" )

# new width and height
W=$(( $SCREEN_WIDTH / 2 - $RIGHTMARGIN ))
H=$(( $SCREEN_HEIGHT - 2 * $TOPMARGIN ))

# moving to the center
X=$(( $SCREEN_WIDTH / 3 ))

if [[ -n ${rotatearound} && -e ${PREVIOUS} ]]; then
    previous=$(cat ${PREVIOUS})
    if [[ ${previous} == "gauche" ]];then
        X=${SCREEN_WIDTH}
        echo "droite" > ${PREVIOUS}
    elif [[ ${previous} == "droite" ]];then
        X=0
        echo "center" > ${PREVIOUS}
    elif [[ ${previous} == "center" ]];then
        echo "gauche" > ${PREVIOUS}
    fi
else
    echo "droite" > ${PREVIOUS}
fi

W=$(( $W + ${ENLARGE_WIDTH} ))
X=$(( $X - ${ENLARGE_WIDTH} ))
Y=$TOPMARGIN

wmctrl -r :ACTIVE: -b remove,maximized_vert,maximized_horz && wmctrl -r :ACTIVE: -e 0,$X,$Y,$W,$H
