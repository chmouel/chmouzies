#!/usr/bin/env bash
# Author: Chmouel Boudjnah <chmouel@chmouel.com>
set -eufo pipefail

[[ -z ${GOPATH:-} ]] && {
  GOPATH="${HOME}/go"
}

PAC_DIR="${GOPATH}/src/github.com/openshift-pipelines/pac/main/docs/content"
TEKTON_DIR="${GOPATH}/src/github.com/tektoncd/pipeline/docs"
DOCS_DIR=("${PAC_DIR}" "${TEKTON_DIR}")

MODEL=${TARGET_MODEL:-gemini:gemini-2.0-flash}
inline=

show_help() {
  cat <<EOF
$(basename $(readlink -f $0)) - pac-answer
 
EOF
  grep -E "[ ]*[a-zA-Z0-9-]\) ##" $0 |
    sed -e 's/^[ ]*/-/' \
      -e 's/-\([0-9A-Za-z]*\)[  ]*|[  ]*\([0-9A-Za-z]*\)/-\1, -\2/' \
      -e 's/##//' -e 's/)[ ]*/ - /' |
    awk -F" - " '{printf "%-10s %s\n", $1, $2}'

  cat <<EOF

Author: Chmouel Boudjnah <chmouel@chmouel.com> - @chmouel
EOF
}

inline_doc() {
  DIR=$1

  # Check if directory is provided
  if [ -z "$DIR" ]; then
    echo "Usage: $0 <directory>"
    exit 1
  fi

  # Function to process each Markdown file
  process_markdown_file() {
    local file=$1
    echo "Processing file: $file"
    # Read the file content
    while IFS= read -r line; do
      echo "$line"
    done <"$file"
    echo "---"
  }

  # Export the function to be used with find
  export -f process_markdown_file

  # Find all Markdown files and process them
  find "$DIR" -type f -name "*.md" -exec bash -c 'process_markdown_file "$0"' {} \;
}

while getopts "hi" opt; do
  case $opt in
  i)
    if [ ! -t 1 ]; then
      choice=${PAC_DIR}
    else
      choice=$(gum choose ${PAC_DIR} ${TEKTON_DIR})
    fi
    inline_doc ${choice}
    exit 0
    ;;
  h)
    echo "usage: $(basename $(readlink -f $0))"
    show_help
    exit 0
    ;;
  *)
    echo "unknown option: -${OPTARG}" >&2
    show_help
    exit 1
    ;;
  esac
done
shift $((OPTIND - 1))

ARG="${1:-}"
if [[ -e ${ARG} ]]; then
  QUESTION=$(cat ${ARG})
elif [[ -n ${ARG} ]]; then
  QUESTION=${ARG}
else
  read -p "Question: " QUESTION
fi

prompt="can you answer the question for me don't mention that you are an AI.
Be courteous.
Be professional
Be brief in your answer.

Here is the quesitons:

${QUESTION}
"

args=()
for dir in "${DOCS_DIR[@]}"; do
  args+=("-f" "${dir}")
done

aichat -m ${MODEL} "${args[@]}" "${prompt}"
