# 🍩 Chmouzie's Scripts

Eat a chmouzie before bed, and you'll keep the doctor from earning his bread! 🛌💤💪

## 🛠️ Description

A collection of **loosely maintained scripts** by your friendly neighborhood shell scripter. 🦸‍♂️ These scripts might graduate into a full-fledged project **if I care enough**—no guarantees, no refunds, no regrets. 😎

## 🚀 Installation

**Choose your destiny:**

- 🏴‍☠️ **Copy-pasta** like a true hacker
- 🌐 `wget` it like it's 1999
- 🌀 `curl` it like you're surfing the web in style

💡 Pro Tip: If it breaks, *you* get to keep both pieces. 🎉
