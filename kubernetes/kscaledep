#!/usr/bin/env bash
# Chmouel Boudjnah <chmouel@chmouel.com>
# Scale on and off deployments
namespace=

print_usage() {
	echo "Usage: $(basename 0) -n namespace"
}

while getopts "hn:" o; do
	case "${o}" in
	n)
		namespace="-n ${OPTARG}"
		;;
	h)
		print_usage
		exit 0
		;;
	*)
		echo "Invalid option"
		print_usage
		exit 1
		;;
	esac
done
shift $((OPTIND - 1))

arg=${1:-""}
forced=${2-""}

deployment=$(kubectl get ${namespace} deployments.apps | fzf -1 --height=6 --header-lines=1 --reverse -q "${arg}")
[[ -z ${deployment} ]] && exit 0

deployname=$(echo ${deployment} | cut -d" " -f1)
deployed=$(echo ${deployment} | cut -d" " -f2)
deployed=${deployed/\/*/}

scaleto() {
	echo "Scaling ${1} to ${2}"
	kubectl ${namespace} scale --replicas=${2} deployment ${1}
}

if [[ ${forced} ]]; then
	scaleto ${deployname} ${forced}
elif ((deployed)); then
	scaleto ${deployname} 0
else
	scaleto ${deployname} 1
fi
