#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Chmouel Boudjnah <chmouel@chmouel.com>
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
import base64
import os
import sys

import yaml
from yaml import CLoader as Loader

if len(sys.argv) == 1:
    stream = sys.stdin
else:
    # pylint: disable=consider-using-with
    stream = open(sys.argv[1], encoding="utf-8")

ydata = yaml.load(stream, Loader=Loader)
dico = {}
if "items" not in ydata:
    dico["items"] = [ydata]
else:
    dico = ydata

if "items" not in dico or not isinstance(dico["items"], list):
    print("invalid yaml?")
    sys.exit(1)

COLORIT = sys.stdout.isatty() and sys.stderr.isatty()
if "FORCE_COLOR" in os.environ:
    COLORIT = True


# output a string from a 256 color palette
def colorize(string, color):
    # detect if we are in a terminal or output no color
    if COLORIT:
        return "\033[38;5;%dm%s\033[0m" % (int(color), string)
    else:
        return string


for item in dico["items"]:
    if item != dico["items"][0]:
        print()
    s = colorize("Secret", 170) + ": " + colorize(item["metadata"]["name"], 34)
    print(s)
    ll = len(s)
    if COLORIT:
        ll = ll - 30
    print(colorize("=", 170) * ll)
    print()
    for key in item:
        if not key.strip() or key != "data":
            continue
        datas = item[key].items()
        last = list(datas)[-1][0]
        for d in datas:
            if not d[1]:
                continue
            decoded = base64.b64decode(d[1]).decode("utf-8")
            print(f"{colorize('Key', '202')}: {d[0]}")
            if len(decoded) > 50:
                print(f"{colorize('Value', '202')}:\n{decoded.strip()}")
            else:
                print(f"{colorize('Value', '202')}: {decoded.strip()}")
            if d[0] != last:
                print("\n")
