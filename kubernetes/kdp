#!/usr/bin/env zsh
# shellcheck shell=bash
# Copyright 2024 Chmouel Boudjnah <chmouel@chmouel.com>
#
# Describe pod or pipelineruns, with kubectl describe
# select it with fzf (using kselect from this repo)
# can get only labels/annotations
# can show the output in $EDITOR (to easily copy/search and paste stuff that we need)
# can show the output in fzf to easily filter stuff

ns="" label="" output="" selection="" st="" labelninja="" nofzf="" otype=""

function help() {
    cat <<EOF
Usage: kdp [options] [podname]

Options:
 -n namespace
 -l show labels
 -L show labels in a ninja way (for easy copy/paste) 🥷
 -s show status
 -E show events
 -a show service account
 -F don't end up showing in fzf
 -e edit in \$EDITOR
 -r raw output
 -t type (ie: PipelineRun)
 -h help
EOF
}

zparseopts -D -E -F - E=events t:=otype s=st a=serviceaccount n:=ns r=rawoutput L=labelninja l=label F=nofzf e=edit h=help || { help; exit 1 }

end_opts=$@[(i)(--|-)]
set -- "${@[0,end_opts-1]}" "${@[end_opts+1,-1]}"

[[ -n ${help} ]] && { help; exit 0 }
otype=${${otype#-t}# }
otype=${otype:-pod}
set -u
selection=$(kselect ${ns[@]} ${otype} ${@})


[[ -n $selection ]] || exit 0
if [[ -n ${rawoutput} ]]; then
    output=$(kubectl get ${ns[@]} ${otype} ${selection} -o yaml) ;
    if [[ -n ${edit} ]]; then
        tempfile=$(mktemp /tmp/.kubedesc.XXXXXX)
        trap "rm -f ${tempfile}" EXIT
        echo ${output} > ${tempfile}
        ${EDITOR:-vi} ${tempfile}
    else
        printf '%s\n' "${output}"
    fi
fi

output=$(kubectl describe ${ns[@]} ${otype} ${selection})
[[ -n $output ]] || exit 0

case true in
    ${labelninja})
        output=$(printf '%s\n' "${output}" | sed -n '/Labels/,/^Status/ { s/=/ = /;p }')
        ;;
    ${events})
        output=$(printf '%s\n' "${output}" | sed -n '/Events:/,/^$/ { p }')
        ;;
    ${label})
        output=$(printf '%s\n' "${output}" | sed -n '/Labels/,/^Status/ { p }')
        ;;
    ${st})
        output=$(printf '%s\n' "${output}" | sed -n '/Status:/ { s/.*:[ \t]*\(.*\)$/\L\1/; p; }')
        ;;
    ${serviceaccount})
        output=$(printf '%s\n' "${output}" | sed -n '/Service Account:/ { s/.*:[ \t]*\(.*\)$/\L\1/; p; }')
        ;;
esac

if [[ -n ${edit} ]]; then
    tempfile=$(mktemp /tmp/.kubedesc.XXXXXX)
    trap "rm -f ${tempfile}" EXIT
    echo ${output} > ${tempfile}
    ${EDITOR:-vi} ${tempfile}
elif [[ -n ${labelninja} || -n ${events} || -n ${label} || -n ${st} || -n ${serviceaccount} ]]; then
    printf '%s\n' "${output}"
elif [[ -z ${nofzf} ]]; then
    finally=$(printf '%s\n' "${output}" | fzf --no-sort --reverse --bind "Ctrl-c:execute-silent(echo {} | sed -e 's/^[ \t]*//' | pbcopy)+abort")
    [[ -n ${finally} ]] && { echo ${finally} | sed -e 's/^[ \t]*//'; }
    exit
else
    printf '%s\n' "${output}"
fi
