#!/usr/bin/env bash
set -eu

FD=fd
type -p fdfind >/dev/null 2>/dev/null && FD=fdfind

namespace=
extra_create_args=
sleepit=0.5
user=

while getopts "u:fn:s:" o; do
	case "${o}" in
	u)
		user="--as ${OPTARG}"
		;;
	s)
		sleepit=${OPTARG}
		;;
	f)
		extra_create_args="${extra_create_args} --validate=false"
		;;
	n)
		namespace="${OPTARG}"
		;;
	*)
		echo "Invalid option"
		exit 1
		;;
	esac
done
shift $((OPTIND - 1))
[[ -n ${namespace} ]] && namespace="-n ${namespace}"

args=${@:-}
farg=${1:-}
q=""
[[ -z ${args} || ! -f ${farg} ]] && {
	[[ -n ${farg} ]] && q="-q ${farg}"
	args=$(${FD} -t f '.*\.(y(a?)ml|json)' | fzf ${q})
}

for file in $args; do
	maybesleepit=
	echo -n "Trying to delete ${file}: "
	if kubectl ${user} ${namespace} delete -f ${file} --now 2>/dev/null; then
		maybesleepit=yes
	else
		true
		echo "not found"
	fi
	ov=${extra_create_args}
	grep -q 'Route' ${file} && extra_create_args="--validate=false"
	[[ -n ${sleepit} && -n ${maybesleepit} ]] && sleep ${sleepit}
	echo "Creating ${file}"
	kubectl ${user} ${namespace} ${extra_create_args} create -f ${file}
	extra_create_args=${ov}
done
