#!/usr/bin/env bash
# Copyright 2023 Chmouel Boudjnah <chmouel@chmouel.com>
set -eufo pipefail

farg=""
all=""
klogopt=""
kgetpodopt=""
kdeleteopt=""
onlypod=
greppattern=""
delete=
snazy="yes"
nofollow=

[[ $(basename $0) == GP ]] && onlypod=yes

function help() {
	cat <<EOF
ocla /container substring to filter/

This will let you choose a pod with fzf and show logs

-S do not use snazy
-o don't show logs, only output pod
-F do not follow logs
-r filter to only running pods
-1 choose the first one found in substring
-a show all pods from the namespaces via kail
-n use that namespace
-k delete the pod
-g grep a pattern
-l label selector
-h this help
-1 choose the first one found in substring

EOF
}

while getopts "ag:n:rF1ohkl:S" o; do
	case "${o}" in
	a)
		all=yes
		;;
	n)
		kgetpodopt="${kgetpodopt} -n ${OPTARG}"
		klogopt="${klogopt} -n ${OPTARG}"
		kdeleteopt="${klogopt} -n ${OPTARG}"
		;;
	o)
		onlypod=yes
		;;
	1)
		farg="${farg} -1"
		;;
	k)
		delete=true
		;;
	l)
		kgetpodopt="${kgetpodopt} -l ${OPTARG}"
		;;
	r)
		kgetpodopt="${kgetpodopt} --field-selector status.phase=Running"
		;;
	F)
		nofollow="yes"
		;;
	g)
		greppattern="${OPTARG}"
		;;
	S)
		snazy=
		;;
	h)
		help
		exit 0
		;;
	*)
		echo "Invalid option"
		help
		exit 1
		;;
	esac
done
shift $((OPTIND - 1))

ARG1=${1:-}
if [[ -n ${ARG1} ]]; then
	farg="${farg} -q ${ARG1}"
	shift
fi

[[ -z ${nofollow} ]] && {
	klogopt="${klogopt} -f"
}

if [[ -n ${all} ]]; then
	kcmd=(kail --since=10m)
	if [[ -n ${greppattern} ]]; then
		"${kcmd[@]}" | snazy -r "${greppattern}"
	elif [[ -n ${snazy} ]]; then
		"${kcmd[@]}" | snazy
	else
		"${kcmd[@]}"
	fi
	exit
fi

PODS=$(kubectl get pod --no-headers ${kgetpodopt} --sort-by=.metadata.creationTimestamp | fzf --multi -1 --tac --layout=reverse ${farg} --height=10% | awk '{print $1}')
if [[ -z ${PODS} ]]; then
	exit
fi

if [[ -n ${delete} ]]; then
	fpods=""
	for i in ${PODS}; do
		fpods="${fpods} pods/${i}"
	done
	kubectl delete ${kdeleteopt} ${fpods}
	exit
fi

for pod in ${PODS}; do
	if [[ -n ${onlypod} ]]; then
		echo $pod
	else
		if [[ -n ${greppattern} ]]; then
			kubectl logs --max-log-requests=20 --all-containers ${pod} ${klogopt} | snazy -r "${greppattern}"
		elif [[ -n ${snazy} ]]; then
			kubectl logs --max-log-requests=20 --all-containers ${pod} ${klogopt} | snazy
		else
			kubectl logs --max-log-requests=20 --all-containers ${pod} ${klogopt}
		fi
	fi
done
