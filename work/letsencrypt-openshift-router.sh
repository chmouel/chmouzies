#!/usr/bin/env bash
# Chmouel Boudjnah <chmouel@chmouel.com>
set -euo pipefail

profile=
dnsplugin=aws
api_server=
apps_domain=
target_dir=$HOME/.acme.sh/certs
ignore_issue_error=yes
args=""
do_openshift_router_install=yes
renew=

# let not save things in account.conf
export _using_role=true

[[ -e ${HOME}/.acme.sh/acme.sh ]] || {
	echo "You need to install acme.sh"
	exit 1
}

function print_help() {
	cat <<EOF
Usage: $0 [options]

-d               debug
-s               use le staging servers when testing things out
-F               don't ignore issue error
-r               renew
-R               don't do openshift router install
-f               force renew
-p PROFILE       aws profile from pass keys aws/ prefix
-c AWS_CRED_FILE credential file to use
-t DIR           target directory
-a API_SERVER    api server (detected otherwise)
-w API_DOMAIN    apps domain (detected otherwise)
-g               use gcloud dns plugin
-h               this help
EOF
}

while getopts "dhtc:Rp:ga:w:frFs" o; do
	case "${o}" in
	d)
		args="--debug ${args}"
		set -x
		;;
	s)
		args="--staging ${args}"
		do_openshift_router_install=
		;;
	F)
		ignore_issue_error=
		;;
	r)
		renew=true
		;;
	R)
		do_openshift_router_install=
		;;
	t)
		target_dir=${OPTARG}
		;;
	f)
		args="${args} --force"
		;;
	a)
		api_server=${OPTARG}
		;;
	w)
		apps_domain=${OPTARG}
		;;
	c)
		credential_file=${OPTARG}
		[[ -f ${credential_file} ]] || {
			echo "${credential_file} does not exist"
			exit 1
		}
		eval $(sed -rn '/^aws/ { s/([^ ]*)[ ]*=[ ]*/\U\1=/;s/^/export /;p ;}' <${credential_file})
		[[ -z ${AWS_ACCESS_KEY_ID:-""} || -z ${AWS_SECRET_ACCESS_KEY:-""} ]] && {
			echo "could not get keys from credential_file"
			exit 1
		}
		;;
	p)
		profile=${OPTARG}
		export AWS_ACCESS_KEY_ID=$(pass show "aws/${profile}/key_id")
		export AWS_SECRET_ACCESS_KEY=$(pass show "aws/${profile}/access_key")
		;;
	g)
		dnsplugin=gcloud
		;;
	h)
		print_help
		exit 0
		;;
	*)
		echo "Invalid option"
		echo ""
		print_help
		exit 1
		;;
	esac
done
shift $((OPTIND - 1))

[[ -z ${api_server} ]] && api_server=$(oc whoami --show-server | sed 's,.*://,,;s/:[0-9]*//')
target_dir=${target_dir}/${api_server}
export_dir=${target_dir}/exported
mkdir -p "${target_dir}" "${export_dir}"
export DOMAIN_PATH=${target_dir}

[[ -n ${renew} ]] && {
	"${HOME}/.acme.sh/acme.sh" --renew --domain "${api_server}" ${args}
	exit 0
}

[[ -z ${apps_domain} ]] && apps_domain=$(kubectl get ingresscontroller default -n openshift-ingress-operator -o jsonpath='{.status.domain}')

[[ -z ${api_server} || -z ${apps_domain} ]] && {
	echo "no api_server or apps_domain set"
	exit 1
}
echo "Issuing for domain: ${api_server}"

[[ -n ${apps_domain} ]] && {
	echo "Issuing for wildcard *.${apps_domain}"
	args="$args -d *.${apps_domain}"
}

"$HOME/.acme.sh/acme.sh" --issue -d "${api_server}" ${args} --dns dns_${dnsplugin} --server letsencrypt || {
	[[ -n ${ignore_issue_error} ]] || exit 1
}

"${HOME}/.acme.sh/acme.sh" --install-cert -d "${api_server}" ${args} \
	--cert-file "${export_dir}/cert.pem" \
	--key-file "${export_dir}/key.pem" \
	--fullchain-file "${export_dir}/fullchain.pem" \
	--ca-file "${export_dir}/ca.cer"

[[ -n ${do_openshift_router_install} ]] && {
	[[ -z ${apps_domain} ]] && {
		echo "needs apps domain"
		exit 1
	}

	kubectl delete secret router-certs -n openshift-ingress 2>/dev/null || true
	kubectl create secret tls router-certs --cert="${export_dir}/fullchain.pem" \
		--key="${export_dir}/key.pem" -n openshift-ingress
	kubectl patch ingresscontroller default -n openshift-ingress-operator --type=merge \
		--patch='{"spec": { "defaultCertificate": { "name": "router-certs" }}}'
}
