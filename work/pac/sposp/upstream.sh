#!/usr/bin/env bash
# Copyright 2024 Chmouel Boudjnah <chmouel@chmouel.com>
set -euxfo pipefail
export KUBECONFIG=$HOME/.kube/config.chmouel

VERSION=v0.24.6
#VERSION=stable

export PAC_RELEASE_YAML=https://github.com/openshift-pipelines/pipelines-as-code/releases/download/${VERSION}/release.yaml

cd scripts
./sync-kubeconfig.sh
./osp-operator-nightly.sh
./install-upstream.sh
