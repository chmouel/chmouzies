#!/usr/bin/env bash
# Copyright 2024 Chmouel Boudjnah <chmouel@chmouel.com>
set -euxfo pipefail
export KUBECONFIG=$HOME/.kube/config.chmouel

use_release=
dont_sync_kubeconfig=

while getopts "Shr" opt; do
  case $opt in
  r) use_release=yes ;;
  S) dont_sync_kubeconfig=yes ;;
  h)
    echo "usage: prog"
    exit 0
    ;;
  *)
    echo "unknown option: -$OPTARG" >&2
    exit 1
    ;;
  esac
done
shift $((OPTIND - 1))

cd scripts
if [[ -z $dont_sync_kubeconfig ]]; then
  ./sync-kubeconfig.sh
fi
if [[ -n $use_release ]]; then
  ./osp-operator-stable.sh
else
  ./osp-operator-nightly.sh
fi

# shellcheck disable=SC1091
source paac-configure.sh
installMainController
installGHEController
type -p kubens 2>/dev/null >/dev/null || true && kubens openshift-pipelines
version=$(kubectl get operators openshift-pipelines-operator-rh.openshift-operators -o json | jq -r '.status.components.refs[] | select(.kind == "ClusterServiceVersion") | .name | capture("v(?<version>[0-9.]+)").version')
echo "Operator version $version has been installed"
