#!/usr/bin/env bash
# Copyright 2024 Chmouel Boudjnah <chmouel@chmouel.com>
set -euxfo pipefail
export KUBECONFIG=$HOME/.kube/config.chmouel

release=
namespace=pipelines-as-code
dont_sync_operator=
justwatcher=

while getopts "wjShrn:" opt; do
  case $opt in
  n) namespace=$OPTARG ;;
  S) dont_sync_operator=-S ;;
  w) justwatcher=-w ;;
  j)
    cd scripts
    ./install-upstream.sh -j -k -n ${namespace} ${justwatcher}
    exit
    ;;
  h)
    echo "usage: prog"
    exit 0
    ;;
  r) release=-r ;;
  *)
    echo "unknown option: -$OPTARG" >&2
    exit 1
    ;;
  esac
done
shift $((OPTIND - 1))

./operator.sh ${dont_sync_operator} ${release}

cd scripts
./install-upstream.sh -k -n ${namespace}
