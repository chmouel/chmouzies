#!/usr/bin/env bash
# Copyright 2024 Chmouel Boudjnah <chmouel@chmouel.com>
set -euxfo pipefail

BOOTSTRAP_SERVER="https://ssh.chmouel.com/osinstall"
BOOTSTRAP_SERVER="bootstrap"
BOOTSTRAP_PROFILE=${BOOTSTRAP_PROFILE:-"chmouel"}

[[ -z ${HTPASS:-""} ]] && {
  set +e
  type -p pass >/dev/null 2>/dev/null && HTPASS=$(pass show chmouel/htpass || true)
  set -e
}
[[ -z ${HTPASS:-""} ]] && {
  echo "HTPASS not set, set it in envrc or something to username:passwd"
  randomword=$(openssl rand -base64 12 | tr -dc 'a-zA-Z0-9' | fold -w 6 | head -n 1)
  echo "Setting it to pac:${randomword}"
  export HTPASS=pac:${randomword}
}

sync_kubeprofile() {
  # curl -s -o${HOME}/.kube/config.${BOOTSTRAP_PROFILE} ${BOOTSTRAP_SERVER}/${BOOTSTRAP_PROFILE}/kubeconfig
  export KUBECONFIG=${HOME}/.kube/config.${BOOTSTRAP_PROFILE}
  kubectl get nodes >/dev/null 2>/dev/null ||
    scp ${BOOTSTRAP_SERVER}:/home/pipelines/os4-build/profiles/${BOOTSTRAP_PROFILE}/auth/kubeconfig ~/.kube/config.${BOOTSTRAP_PROFILE}
  chmod 0600 ~/.kube/config.${BOOTSTRAP_PROFILE}
}

TMP=$(mktemp /tmp/.mm.XXXXXX)
clean() { rm -f $TMP; }
trap clean EXIT

function create_htpasswd_auth() {
  noadmin=
  [[ -n ${1} && ${1} == -n ]] && {
    noadmin=true
    shift
  }
  username_password=${1}
  username=${username_password%:*}
  password=${username_password#*:}

  htpasswd -B -b -c $TMP $username $password

  kubectl delete secret htpass-secret -n openshift-config || true
  kubectl create secret generic htpass-secret --from-file=htpasswd=$TMP -n openshift-config
  kubectl patch oauth cluster -n openshift-config --type merge --patch "spec:
  identityProviders:
  - htpasswd:
      fileData:
        name: htpass-secret
    mappingMethod: claim
    name: htpasswd
    type: HTPasswd
    " || true
  [[ -z ${noadmin} ]] && oc adm policy add-cluster-role-to-user cluster-admin ${username}
  kubectl delete secrets kubeadmin -n kube-system || true
}

sync_kubeprofile
create_htpasswd_auth ${HTPASS}
create_htpasswd_auth -n test:test
