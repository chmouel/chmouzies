#!/usr/bin/env bash
# Copyright 2024 Chmouel Boudjnah <chmouel@chmouel.com>
export KO_DOCKER_REPO=docker.io/cboudjna

set -euxfo pipefail
[[ -z ${KUBECONFIG-""} ]] && {
  echo "KUBECONFIG not set"
  exit 1
}
namespace=
ko=
reinstallpaac=yes
justwatcher=
KO_TARGET_PLATFORM=${KO_TARGET_PLATFORM:-"linux/arm64"}

while getopts "jwhkn:" opt; do
  case $opt in
  k) ko=yes ;;
  j) reinstallpaac= ;;
  n) namespace=$OPTARG ;;
  w) justwatcher=yes ;;
  h)
    echo "usage: prog"
    exit 0
    ;;
  *)
    echo "unknown option: -$OPTARG" >&2
    exit 1
    ;;
  esac
done
shift $((OPTIND - 1))

if [[ -n $reinstallpaac ]]; then
  (
    export PAC_TARGET_NS=${namespace:-"pipelines-as-code"}
    cd $GOPATH/src/github.com/openshift-pipelines/tekton-asa-code-infra
    ./scripts/pipelineascode.sh
  )
fi

# shellcheck disable=SC1091
source paac-configure.sh
if [[ -n ${ko} ]]; then
  (
    cd $GOPATH/src/github.com/openshift-pipelines/pac/main
    set -x

    target=config/
    if [[ -n ${justwatcher} ]]; then
      target="config/500-watcher.yaml"
    fi
    ko resolve -f ${target} --platform ${KO_TARGET_PLATFORM} | sed "s/namespace: pipelines-as-code/namespace: ${namespace}/" | kubectl apply -f-
    if [[ -z ${justwatcher} ]]; then
      ./hack/second-controller.py --namespace ${namespace} --openshift-route --controller-image=ko ghe-pac | ko apply -f- --platform ${KO_TARGET_PLATFORM}
    fi
  )
  # echo "Applyling Postgres labels"
  # ./postgres.sh -j
fi

if [[ -n $reinstallpaac ]]; then
  export TARGET_NS=${namespace}
  installMainController
  installGHEController ghe-pac-secret

  # echo "Installing Postgres"
  # ./postgres.sh
  type -p kubens 1>/dev/null >/dev/null && kubens pipelines-as-code
fi
