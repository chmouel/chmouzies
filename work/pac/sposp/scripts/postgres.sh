#!/usr/bin/env bash

namespace=pipelines-as-code
password=paac
username=paac
database=paac

applylabels() {
  for component in ghe-pac-controller pipelines-as-code-controller pipelines-as-code-watcher; do
    kubectl set env -n ${namespace} deployment/${component} POSTGRESQL_URI="host=postgresql database=${database} user=${username} password=${password}" || true
  done
}

while getopts "jhn:" opt; do
  case $opt in
  n) namespace=$OPTARG ;;
  j)
    applylabels
    exit
    ;;
  h)
    echo "usage: prog"
    exit 0
    ;;
  *)
    echo "unknown option: -${OPTARG}" >&2
    exit 1
    ;;
  esac
done
shift $((OPTIND - 1))
helm uninstall --wait postgresql --ignore-not-found -n ${namespace}
helm install postgresql --wait \
  --hide-notes \
  --set primary.persistence.enabled=false \
  --set global.postgresql.auth.username=${username} \
  --set global.postgresql.auth.password=${password} \
  --set global.postgresql.auth.database=${database} \
  -n ${namespace} oci://registry-1.docker.io/bitnamicharts/postgresql

applylabels
