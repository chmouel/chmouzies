#!/usr/bin/env bash
oc delete subscription pipelines -n openshift-operators || true
cat <<EOF | oc apply -f-

apiVersion: operators.coreos.com/v1alpha1
kind: Subscription
metadata:
  name: openshift-pipelines-operator
  namespace: openshift-operators
spec:
  channel: latest
  name: openshift-pipelines-operator-rh
  source: redhat-operators
  sourceNamespace: openshift-marketplace
EOF

i=0
for tt in pipelines triggers; do
	while true; do
		[[ ${i} == 120 ]] && exit 1
		ep=$(kubectl get ep -n openshift-pipelines tekton-${tt}-webhook -o jsonpath='{.subsets[*].addresses[*].ip}')
		[[ -n ${ep} ]] && break
		sleep 5
		i=$((i + 1))
	done
done
