#!/usr/bin/env bash
# Copyright 2024 Chmouel Boudjnah <chmouel@chmouel.com>
# need e2e-run to be installed from this repo with the pass secret setup
set -euxfo pipefail
OPENSHIFT=true

[[ -n $(kubectl get pods -n pipelines-as-code -l app=pipelines-as-code-controller 2>/dev/null) ]] && {
  TARGET_NS=pipelines-as-code
  OPENSHIFT=
}
[[ -n $(kubectl get pods -n openshift-pipelines -l app=pipelines-as-code-controller 2>/dev/null) ]] && {
  TARGET_NS=openshift-pipelines
}

[[ -z ${TARGET_NS:-""} ]] && {
  echo "cannot find the installed namespace"
  exit 1
}

[[ -n ${KUBECONFIG-""} ]] || {
  echo "KUBECONFIG not set"
  exit 1
}
kubectl get pipelineruns >/dev/null 2>/dev/null || {
  echo "cannot connect or tekton operator not installed"
  exit 1
}

makeGosmee() {
  local deploymentName=$1
  local smeeURL=$2
  local controllerURL=$3
  local namespace=${4:-gosmee}
  cat <<EOF >/tmp/${deploymentName}.yaml
---
apiVersion: v1
kind: Namespace
metadata:
  name: $namespace
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: $deploymentName
  namespace: $namespace
spec:
  replicas: 1
  selector:
    matchLabels:
      app: gosmee
  template:
    metadata:
      labels:
        app: gosmee
    spec:
      containers:
        - image: ghcr.io/chmouel/gosmee:main
          imagePullPolicy: Always
          name: gosmee
          args:
            [
              "client",
              "--output",
              "json",
              "--saveDir",
              "/tmp/save",
              "$smeeURL",
              "$controllerURL",
            ]
EOF
  kubectl apply -f /tmp/${deploymentName}.yaml
}

getRoute() {
  target=$(kubectl get route -n $TARGET_NS $1 -o jsonpath='{.spec.host}' || true)
  [[ -n ${target} ]] && echo https://${target}
}

installGHEController() {
  local smeeURL passSecret controllerURL
  passSecret=ghe-chmouel
  secretName=${1:-ghe-pac-secret}
  smeeURL=$(pass show github/apps/$passSecret/smee)
  # TODO: non openshift operator
  tt=ghe-pac-controller
  if [[ -n ${OPENSHIFT-""} ]]; then
    kubectl patch tektonconfig config --type="merge" -p '{"spec": {"platforms":{"openshift":{"pipelinesAsCode": {"additionalPACControllers": {"ghe": {"enable": true, "secretName":"ghe-pac-secret"}}}}}}}'
    i=0
    while true; do
      [[ ${i} == 120 ]] && exit 1
      ep=$(kubectl get ep -n ${TARGET_NS} ${tt} -o jsonpath='{.subsets[*].addresses[*].ip}' || true)
      [[ -n ${ep} ]] && break
      sleep 5
      i=$((i + 1))
    done
  fi
  e2e-run -A $passSecret ${secretName} | kubectl apply -f- -n $TARGET_NS
  controllerURL=$(getRoute $tt || true)
  [[ -z ${controllerURL} ]] && {
    controllerURL=http://$tt.$TARGET_NS.svc.cluster.local:8080
  }

  echo "Installing gosmee GHE: $smeeURL $controllerURL"
  makeGosmee $passSecret $smeeURL $controllerURL
}

installMainController() {
  local smeeURL passSecret controllerURL
  passSecret=chmouel-scratchtest
  smeeURL=$(pass show github/apps/$passSecret/smee)
  e2e-run -A $passSecret pipelines-as-code-secret | kubectl apply -f- -n $TARGET_NS
  controllerURL=$(getRoute pipelines-as-code-controller)
  echo "Installing gosmee: $smeeURL $controllerURL"
  makeGosmee $passSecret $smeeURL $controllerURL
}

if [[ $BASH_SOURCE == "$0" ]]; then
  echo installMainController
  echo installGHEController
fi

echo $TARGET_NS
type -p kubens 2>/dev/null >/dev/null && kubens $TARGET_NS
