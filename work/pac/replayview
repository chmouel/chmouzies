#!/usr/bin/env bash
# Copyright 2024 Chmouel Boudjnah <chmouel@chmouel.com>
set -eufo pipefail

interactive=
symlink=
yank=
launch=
directToController=
dir=/tmp/save

echo_magenta() {
  echo -e "\033[35m$*\033[0m"
}

echo_red() {
  echo -e "\033[31m$*\033[0m"
}

echo_yellow() {
  echo -e "\033[33m$*\033[0m"
}

echo_white_bold() {
  echo -e "\033[1;37m$*\033[0m"
}

show_help() {
  cat <<EOF
$(basename $(readlink -f $0)) - view gosme replay files

Usage: $(basename $(readlink -f $0)) [options]

Options:
  -P <file>    Show a preview of the json and file for fzf
  -l <name>    Make a symlink
  -d <dir>     Use a different directory than /tmp/replay
  -y           Copy to clipboard the chosen one
  -r           Run the chosen one
  -c           Do the command directly to the controller
  -i           Interactive mode
  -h           Show this help

Author: Chmouel Boudjnah <chmouel@chmouel.com> - @chmouel
EOF
}

function replayview() {
  local initial=
  [[ -n ${1:-} ]] && initial="-q ${1}"
  local output
  output=$(fd -0 .sh ${dir} | xargs -0 ls -1tr | fzf -1 --tac --reverse --preview='replayview -P {}' --exact --bind alt-n:next-history,alt-p:previous-history,ctrl-j:preview-down,ctrl-k:preview-up,ctrl-n:down,ctrl-p:up,ctrl-d:preview-page-down,ctrl-u:preview-page-up --preview-window 'down,70%:wrap' ${initial})
  [[ -z $output ]] && return
  [[ -n ${symlink} ]] && {
    symlink=${dir}/${symlink}.sh
    ln -fvs ${output} "${symlink}"
    return
  }
  [[ -n $yank ]] && {
    echo $output | pbcopy
    return
  }

  [[ -n ${interactive} ]] && {
    fx ${output/.sh/.json}
    return
  }
  arg="\${1:--l}"
  [[ -n $directToController ]] && arg='"${@}"'
  cat <<EOF >/tmp/run.sh
#!/usr/bin/env bash
set -eufo pipefail
arg="$arg"
[[ \${arg} == "-d" ]] && arg=""
$output \$arg
EOF
  chmod +x /tmp/run.sh
  echo $output
  if [[ -n $launch ]]; then
    bash ${output}
  fi
}

while getopts "il:rnyhcd:P:" opt; do
  case $opt in
  P) ## Show a previuew of the json and file for fzf
    arg=$OPTARG
    j="${arg%.sh}.json"
    [[ -e ${j} ]] || exit
    output=$(grep -Eo "X-((Github|Gitlab)-Event|Event-Key): ([^']*)" "${arg}")
    left=${output%%:*}
    right=${output#*: }
    icon="🔗"
    action=
    case ${left} in
    *Github*)
      icon="$(echo_magenta ) Github"
      if [[ ${right} == "pull_request" ]]; then
        action=":$(jq -r '.action' ${j})"
      fi
      ;;
    *Gitlab*) icon="$(echo_magenta ) Gitlab" ;;
    *Event-Key*) icon="$(echo_magenta ) Bitbucket" ;;
    *) icon="🔗 ${left}" ;;
    esac

    echo ${icon} $(echo_red ${right}${action})

    mapfile -t headers < <(grep -oE -- "-H '[^'\"]*" "${arg}" | sed "s/-H '//" | sort -n)
    for header in "${headers[@]}"; do
      left=${header%%:*}
      right=${header#*: }
      echo " $(echo_yellow ${left}) $(echo_white_bold ${right})"
    done

    echo
    jq -C . ${j}
    grep "curl" ${arg}
    ;;
  l) ## Make a symlink
    symlink=$OPTARG
    ;;
  d) ## Use a different directory than /tmp/replay
    dir=$OPTARG
    ;;
  y) ## Copy to clipboard the chosen one
    yank=yes
    ;;
  r) ## Run the chosen one
    launch=yes ;;
  c) ## Do the command directly to the controller
    directToController=yes ;;
  i) ## Interactive mode
    type -p fx >/dev/null 2>/dev/null || {
      echo "need fx in path"
      exit 1
    }
    interactive=true
    ;;
  h) ## Show this help
    show_help
    exit 0
    ;;
  *)
    echo "unknown option: -$OPTARG" >&2
    show_help
    exit 1
    ;;
  esac
done
shift $((OPTIND - 1))

[[ -d $dir ]] || dir=/tmp/replay
[[ -d $dir ]] || {
  echo "cannot find directory $dir or /tmp/replay" >&2
  exit 1
}
replayview "$@"
