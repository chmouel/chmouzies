I Need to run E2E tests under different kubernetes profiles located in 
~/.kube/config.${KUBEPROFILE_NAME} and with different set of variables
located in my "password store" under the prefix "pac/vars".

The e2e tests output in json logs so I use [snazy](https://github.com/chmouel/snazy) to display them nicely
