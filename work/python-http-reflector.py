#!/usr/bin/env python3
"""
This script creates a simple HTTP server that listens on port 8083.
It receives incoming requests (GET, POST, PUT, DELETE) and prints
detailed information about them to the console, including headers
and payload (formatted as JSON if applicable). It uses only standard
Python libraries and provides colored output for better readability.

(generated with gemini 2.0)
"""

import http.server
import json
import socketserver
import sys
import urllib.parse


class ColorfulHTTPRequestHandler(http.server.SimpleHTTPRequestHandler):
    def log_message(self, format, *args):
        # Disable default logging
        pass

    def do_GET(self):
        self.handle_request()

    def do_POST(self):
        self.handle_request()

    def do_PUT(self):
        self.handle_request()

    def do_DELETE(self):
        self.handle_request()

    def handle_request(self):
        print("\033[1;36m--- Request Start ---\033[0m")  # Cyan

        print("\033[1;32mRequest Line:\033[0m", self.requestline)  # Green

        print("\033[1;34mHeaders:\033[0m")  # Blue
        for key, value in self.headers.items():
            print(f"  \033[34m{key}:\033[0m {value}")

        content_length = int(self.headers.get("Content-Length", 0))
        if content_length > 0:
            print("\033[1;33mPayload:\033[0m")  # Yellow
            try:
                raw_data = self.rfile.read(content_length).decode("utf-8")
                content_type = self.headers.get("Content-Type", "")

                if "application/x-www-form-urlencoded" in content_type:
                    payload = urllib.parse.parse_qs(raw_data)
                    print(json.dumps(payload, indent=4))  # print form data as json
                elif "application/json" in content_type:
                    try:
                        json_data = json.loads(raw_data)
                        pretty_json = json.dumps(json_data, indent=4)
                        print(pretty_json)
                    except json.JSONDecodeError:
                        print(
                            f"\033[0;31mInvalid JSON:\033[0m\n{raw_data}"
                        )  # Red if invalid json
                else:
                    print(raw_data)  # print raw data if not json or form data
            except Exception as e:
                print(f"\033[0;31mError processing payload:\033[0m {e}")  # Red

        print("\033[1;36m--- Request End ---\033[0m\n")  # Cyan

        self.send_response(200)
        self.send_header("Content-type", "text/plain")
        self.end_headers()
        self.wfile.write(b"Request received and printed to console.\n")


if __name__ == "__main__":
    PORT = 8083
    Handler = ColorfulHTTPRequestHandler

    with socketserver.TCPServer(("", PORT), Handler) as httpd:
        print(f"Serving at port {PORT}")
        try:
            httpd.serve_forever()
        except KeyboardInterrupt:
            print("\nServer stopped by user.")
            sys.exit(0)
