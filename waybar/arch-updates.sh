#!/usr/bin/env bash
set -euf

TMP=$(mktemp /tmp/.mm.XXXXXX)
clean() { rm -f $TMP; }
trap clean EXIT

if [[ -n $(command -v checkupdates) ]]; then
	checkupdates >>$TMP
fi

yay -Qu >>$TMP || true
n=$(sort -u $TMP | wc -l)
[[ ${n} -le 0 ]] && exit 0

tooltip=$(sort -u $TMP | sed ':a;N;$!ba')
jq --unbuffered --compact-output -n \
	--arg text " ${n} " \
	--arg alt "Updates" \
	--arg tooltip "$tooltip" \
	--arg class "" \
	--arg percentage "1" \
	'{text: $text, alt: $alt, tooltip: $tooltip, class: $class, percentage: $percentage}'
