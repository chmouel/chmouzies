#!/usr/bin/env python3
import argparse
import shutil
import subprocess
import sys
from datetime import datetime

import requests

# command line arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    "key", help="API key to retrieve weather data", default="", nargs="?"
)
parser.add_argument(
    "-f",
    action="store",
    dest="ftime",
    type=float,
    help="show forecast for given number of hours in advance",
)
parser.add_argument(
    "-i", "--imperial", action="store_true", help="switch to imperial units"
)
parser.add_argument(
    "--latitude", action="store", dest="lat", help="use latitude of a location"
)
parser.add_argument(
    "--longitude", action="store", dest="long", help="use longitude of a location"
)
parser.add_argument(
    "-p", "--place", action="store_true", help="show location of weather station"
)
parser.add_argument(
    "-t", "--text", action="store_true", help="print text instead of symbols"
)
args = parser.parse_args()

# API key assignment via argument
if args.key:
    api_key = args.key
elif shutil.which("pass"):
    api_key = (
        subprocess.Popen(
            ["pass", "show", "openweathermap/apikey"], stdout=subprocess.PIPE
        )
        .communicate()[0]
        .decode()
        .strip()
    )
else:
    sys.exit("No API key given.")

# forecast time argument
if args.ftime:
    if 0 <= float(args.ftime) < 120:
        listID = int(round(args.ftime)) / 3
    elif float(args.ftime) == 120:
        listID = 39
    else:
        sys.exit("Forecast from 0 (now) up to 120 hours.")
else:
    listID = 0

# temperature unit
units = "imperial" if args.imperial else "metric"

# URL of a location API provider
url_locater = "http://ip-api.com/json"


# function to get geo location from a provider in case of not given as an argument
def get_location_data():
    try:
        ld = requests.get(url_locater, timeout=9)
    except Exception:
        sys.exit(1)
    return ld.json()


location = get_location_data()

# assign geo location either via argument (first) or location API provider (last)
lat = args.lat if args.lat else str(location["lat"])
lon = args.long if args.long else str(location["lon"])

# URL of a weather forecast provider (formatted for OpenWeatherMap API)
url_forecast = f"https://api.openweathermap.org/data/2.5/forecast?appid={api_key}&lat={lat}&lon={lon}&units={units}"


# function for an API call for weather forecasts (OpenWeatherMap)
def get_api_data(url):
    try:
        ad = requests.get(url, timeout=9)
    except requests.exceptions.Timeout:
        sys.exit("Connection timed out.")
    except requests.exceptions.TooManyRedirects:
        sys.exit("Tried too many times.")
    except requests.exceptions.HTTPError as herr:
        sys.exit(herr)
    except requests.exceptions.RequestException as reerr:
        sys.exit(reerr)
    return ad.json()


# variables for JSON request
forecast = get_api_data(url_forecast)

# getting errors from OWM directly? (specific)
if forecast["cod"] in [400, 401, 404, 429]:
    sys.exit(forecast["message"] + ".")

# variable assignment for print (OWM specific)
conditionID = forecast["list"][listID]["weather"][0]["id"]
conditionMain = forecast["list"][listID]["weather"][0]["main"]
cityForecast = forecast["city"]["name"]
tempForecast = str("%.0f" % round(forecast["list"][listID]["main"]["temp"]))
timeUnixForecast = forecast["list"][listID]["dt"]
timeForecast = datetime.fromtimestamp(timeUnixForecast).strftime("%H:%M")

# daylight? (OWM specific)
daylight = forecast["list"][listID]["sys"]["pod"] == "d"

# Unicode clock symbol assignment
clock_symbols = {
    "00:00": "\ue381",
    "12:00": "\ue381",
    "01:00": "\ue382",
    "13:00": "\ue382",
    "02:00": "\ue383",
    "14:00": "\ue383",
    "03:00": "\ue384",
    "15:00": "\ue384",
    "04:00": "\ue385",
    "16:00": "\ue385",
    "05:00": "\ue386",
    "17:00": "\ue386",
    "06:00": "\ue387",
    "18:00": "\ue387",
    "07:00": "\ue388",
    "19:00": "\ue388",
    "08:00": "\ue389",
    "20:00": "\ue389",
    "09:00": "\ue38a",
    "21:00": "\ue38a",
    "10:00": "\ue38b",
    "22:00": "\ue38b",
    "11:00": "\ue38c",
    "23:00": "\ue38c",
}
symbol_clk = clock_symbols.get(timeForecast, "\uf527")

# output as text only (without Unicode symbols)
if args.text:
    temp_unit = "degF" if args.imperial else "degC"
    if args.place:
        print(
            f"{conditionMain} at {tempForecast} {temp_unit} in {cityForecast} for {timeForecast}"
        )
    else:
        print(f"{conditionMain} at {tempForecast} {temp_unit} for {timeForecast}")

# Unicode condition symbol assignment depending on API condition IDs and daytime (OWM specific)
else:
    condition_symbols = {
        200: "\ue30e" if daylight else "\ue337",
        210: "\ue30e" if daylight else "\ue337",
        300: "\ue309" if daylight else "\ue334",
        310: "\ue309" if daylight else "\ue334",
        500: "\ue308" if daylight else "\ue333",
        501: "\ue308" if daylight else "\ue333",
        600: "\ue308" if daylight else "\ue333",
        800: "\ue30d" if daylight else "\ue32b",
        801: "\ue30c" if daylight else "\ue37b",
        802: "\ue302" if daylight else "\ue32e",
        803: "\ue312",
        804: "\ue33d",
    }
    condition_symbols.update(
        {
            "Thunderstorm": "\ue31d",
            "Drizzle": "\ue319",
            "Rain": "\ue318",
            "Snow": "\ue318",
            "Mist": "\uf75f",
            "Smoke": "\ue35c",
            "Haze": "\ue3ae" if daylight else "\ue37b",
            731: "\ue301" if daylight else "\ue32d",
            "Fog": "\ue303" if daylight else "\ue346",
            "Sand": "\ue37a",
            "Dust": "\ue35d",
            "Ash": "\ue3c0",
            "Squall": "\ue3c6",
            "Tornado": "\ue351",
        }
    )
    symbol_cond = condition_symbols.get(
        conditionID, condition_symbols.get(conditionMain, "\ue345")
    )

    # output with symbols
    if args.place:
        temp_unit = "\u00b0F" if args.imperial else "\u00b0C"
        print(f"{symbol_clk} {symbol_cond} {tempForecast}{temp_unit} in {cityForecast}")
    else:
        temp_unit = "\ue341" if args.imperial else "\ue339"
        print(f"{symbol_clk} {symbol_cond} {tempForecast}{temp_unit}")
