#!/usr/bin/env bash
# Copyright 2022 Chmouel Boudjnah <chmouel@chmouel.com>
set -eufo pipefail

running=$(kind get clusters 2>/dev/null)

start() {
	kitty-ctrl jump -t "Install Kind" "$GOPATH/src/github.com/openshift-pipelines/pac/main/hack/dev/kind/install.sh;read -p 'Press key to exit' -n1"
	running=yes
}

if [[ ${1:-""} == "click" ]]; then
	if [[ -n ${running} ]]; then
		kitty-ctrl jump -t "Stop Kind" "stopkind ; read -n1 -p 'Press Key to exit'"
	else
		start
	fi
elif [[ ${1:-""} == "reinstall" ]]; then
	start
fi

if [[ -n ${running} ]]; then
	echo "<span foreground=\"cyan\">K </span>"
fi
