#!/usr/bin/env bash
info=$(nmcli -t connect show --active | awk -F: '{ if ($3 == "vpn") print $1; }')

if [[ ${1} == "click" ]]; then
  if [[ -n ${info} ]]; then
    nmcli connection down "${info}"
    notify-send -i ~/.local/share/icons/disconnected.svg "Disconnected to ${info}" "We have succesfully disconnected from ${info}"
  else
    info=$(nmcli -t connect show | awk -F: '{ if ($3 == "vpn") print $1; }' | sed 's/ //g')
    [[ -n ${info} ]] && {
      rhpass -v -g
    }
  fi
  exit
fi

[[ -n ${info} ]] && {
  case ${info} in
  Red\ Hat)
    echo "RHT"
    ;;
  *)
    echo "${info}"
    ;;
  esac
}
