#!/usr/bin/env bash

for source in "$@"; do
  case $source in
  *.yaml | *.yml)
    yq -P . "$source" | bat -l yaml --color=always
    ;;
  *.tar)
    tar -tvf "$source"
    ;;
  *.tgz | *.tar.gz)
    tar -tzvf "$source"
    ;;
  *.json.gz)
    gzcat "$source" | jq '.' | bat -l json
    ;;
  *.json.bz2)
    bzcat "$source" | jq '.' | bat -l json --color=always
    ;;
  *.json.xz)
    xzcat "$source" | jq '.' | bat -l json --color=always
    ;;
  *.json.zst)
    zstdcat "$source" | jq '.' | bat -l json --color=always
    ;;
  *.zst)
    zstdcat "${source}"
    ;;
  *.gz)
    gzcat "$source"
    ;;
  *.bz2)
    bzcat "$source"
    ;;
  *.xz)
    xzcat "$source"
    ;;
  *.json)
    jq '.' "$source" -C
    ;;
  *) bat --color=always "$source" ;;
  esac
done
