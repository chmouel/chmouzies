#!/usr/bin/env bash
# Author: Chmouel Boudjnah <chmouel@chmouel.com>
set -euxfo pipefail

TMP="/tmp/launchcmd.log"
echo >${TMP}

declare -A commands
commands=(
  ['Delete Kubernetes Watcher Lease']='export KUBECONFIG=$(zsh -c "source $HOME/.cache/zsh/repos/chmouel/kubectl-config-switcher/kubectl-config-switcher.plugin.zsh; kcs -p");kubectl delete leases.coordination.k8s.io pac-watcher.github.com.openshift-pipelines.pipelines-as-code.pkg.reconciler.reconciler.00-of-01 -n pipelines-as-code'
  ['Launch last run']="/tmp/run.sh"
  ['Cleanup Old PR']='export KUBECONFIG=$(zsh -c "source $HOME/.cache/zsh/repos/chmouel/kubectl-config-switcher/kubectl-config-switcher.plugin.zsh; kcs -p");cd ~/Sync/paac/github/repos/ghe/chmouel/stress-testing/;./cleanup.sh'
  ['Restart logid']="sudo systemctl restart logid;journalctl -u logid --lines=2 -o cat"
)
IFS=$'\n'
ret=$(
  for cmd in "${!commands[@]}"; do
    echo -e "• ${cmd}"
  done | fuzzel -d
)
[[ -z $ret ]] && exit 1
ret=${ret:2}

export POWERLEVEL9K_DISABLE_GITSTATUS=true
env POWERLEVEL9K_DISABLE_GITSTATUS=true zsh -l -c "export POWERLEVEL9K_DISABLE_GITSTATUS=true;${commands[$ret]}" 2>&1 | tee ${TMP}
[[ -s ${TMP} ]] || exit 0

notify-send --icon=dialog-information --expire-time=5000 "${ret}" "$(
  echo
  cat ${TMP}
)"
