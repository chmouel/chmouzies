#!/usr/bin/env python3
# Author: Chmouel Boudjnah <chmouel@chmouel.com>
# Replace string inside files with per-change interactive mode and difflib
#
# Requires: Python 3.10+ and fd
#
# You can do this with Emacs directly with:
#
# 1. Open Emacs.
# 2. Press `C-x d` to open `dired` mode.
# 3. Navigate to the directory containing the files.
# 4. Press `M-x` and type `find-name-dired` to find files matching a pattern.
# 5. Mark the files you want to edit by pressing `m`.
# 6. Press `Q` to run `dired-do-query-replace-regexp` on the marked files.
# 7. Enter the search and replacement patterns as before.
#
# Or with bash
#
# sd() {
#   local glob_pattern="$1"
#   local search_pattern="$2"
#   local replace_pattern="$3"
#
#   fd -g "$glob_pattern" -x python3 -c "
# import sys, re
# from pathlib import Path
#
# file = Path(sys.argv[1])
# text = file.read_text()
# file.write_text(re.sub(r'$search_pattern', '$replace_pattern', text))
# " {}
# }
#
# other better tools if you value fastness is the original sd https://github.com/chmln/sd
# or sad with interactiveness https://github.com/ms-jpq/sad
#
# but mine is nicer to my workfklow and I like it better.

import argparse
import difflib
import os
import re
import subprocess
import sys

ENCODING = "utf-8"


def print_help():
    """Print usage instructions and examples."""
    program = os.path.basename(sys.argv[0])
    help_text = f"""
Usage: {program} [OPTIONS] [FILE_PATTERN] SEARCH_PATTERN REPLACEMENT_PATTERN

Search and replace text in files using regular expressions.

You will need fd for file search unless you specify a static list in input.

Arguments:
    FILE_PATTERN          Pattern to match files (e.g., '*.txt')
                          If omitted, reads file list from stdin
    SEARCH_PATTERN        Regular expression pattern to search for
    REPLACEMENT_PATTERN   Text to replace matched patterns with

Options:
    -q, --quiet           Suppress output to stdout
    -h, --help            Show this help message and exit
    -i, --interactive     Ask before changing each match in a file and show a diff
    -I, --no-ignore-files Do not try to read .ignore files to exclude files
    --dry-run             Show changes without modifying files
    --encoding ENCODING   Specify file encoding (default: utf-8)

Examples:
    {program} '.txt$' 'hello' 'world'           # Replace in all .txt files
    {program} '.py$' 'hello (.*)' '\\1 hello'   # Use capture groups in replacement
    cat filelist.txt | {program} 'old' 'new'    # Read files from stdin

The list of modified files are printed to stdout.
    """
    print(help_text.strip())


def confirm_change(
    filename, match, replacement, line, line_number, match_count, total_matches
):
    """Ask the user to confirm a specific change with a diff."""
    start, end = match.span()
    old_line = line
    new_line = line[:start] + replacement + line[end:]

    # Generate a diff
    diff = difflib.unified_diff(
        [old_line], [new_line], fromfile="Original", tofile="Modified", lineterm=""
    )

    # Print the diff
    print(
        f"Match {match_count} of {total_matches} found in file {filename} (line {line_number}):"
    )
    for line in diff:
        line = line.rstrip()
        if not line:
            continue
        if line.startswith("+++") or line.startswith("---") or line.startswith("@@"):
            continue
        elif line.startswith("+"):
            print(f"\033[92m{line}\033[0m")  # Green for additions
        elif line.startswith("-"):
            print(f"\033[91m{line}\033[0m")  # Red for deletions
        else:
            print(line)

    response = input("Do you want to apply this change? [Y/n] ")
    return response.lower() in ("", "y")


def parse_args():
    parser = argparse.ArgumentParser(
        description="Search and replace text in files using regular expressions.",
        add_help=False,
    )
    parser.add_argument(
        "file_pattern",
        nargs="?",
        help="Pattern to match files (e.g., '*.txt'). If omitted, reads file list from stdin.",
    )
    parser.add_argument(
        "--no-ignore-files",
        "-I",
        action="store_true",
        help="Do not try to read .ignore files to exclude files.",
    )
    parser.add_argument(
        "search_pattern", help="Regular expression pattern to search for."
    )
    parser.add_argument(
        "replacement_pattern", help="Text to replace matched patterns with."
    )
    parser.add_argument(
        "-q", "--quiet", action="store_true", help="Suppress output to stdout."
    )
    parser.add_argument(
        "-h", "--help", action="store_true", help="Show this help message and exit."
    )
    parser.add_argument(
        "-i",
        "--interactive",
        action="store_true",
        help="Ask before changing each match in a file and show a diff.",
    )
    parser.add_argument(
        "--dry-run",
        action="store_true",
        help="Show changes without modifying files.",
    )
    args = parser.parse_args()
    return args


def compile_search_pattern(pattern):
    try:
        return re.compile(pattern)
    except re.error as e:
        print(f"Invalid search pattern regex: {e}")
        sys.exit(1)


def find_matching_files(no_ignore_files, file_pattern):
    ignore_files = set()
    fd_command = ["fd"]
    if file_pattern:
        fd_command.append("-tf")
        fd_command.append(file_pattern)
    if no_ignore_files:
        fd_command.append("--no-ignore")
    fd_command.append(".")

    result = subprocess.run(fd_command, capture_output=True, text=True)
    all_files = result.stdout.splitlines()

    return [f for f in all_files if f not in ignore_files]


def read_file(path, encoding):
    try:
        with open(path, "r", encoding=encoding) as file:
            return file.readlines()
    except Exception as e:
        print(f"Error reading file {path}: {e}")
        return None


def write_file(path, lines, encoding):
    try:
        with open(path, "w", encoding=encoding) as file:
            file.writelines(lines)
        print(f"Modified file: {path}")
    except Exception as e:
        print(f"Error writing file {path}: {e}")


def is_binary_file(path):
    result = subprocess.run(["grep", "-lI", "", path], capture_output=True)
    return result.returncode != 0


def process_file(
    path, search_regex, replacement_pattern, encoding, interactive, dry_run
):
    if is_binary_file(path):
        return False

    lines = read_file(path, encoding)
    if lines is None:
        return False

    new_lines = []
    file_modified = False
    total_matches = sum(len(list(search_regex.finditer(line))) for line in lines)
    match_count = 0

    def replacement_function(match):
        return match.expand(replacement_pattern)

    for line_number, line in enumerate(lines, 1):
        matches = list(search_regex.finditer(line))
        if not matches:
            new_lines.append(line)
            continue

        current_line = line
        for match in matches:
            match_count += 1
            if interactive and not confirm_change(
                path,
                match,
                replacement_pattern,
                line,
                line_number,
                match_count,
                total_matches,
            ):
                continue

            start, end = match.span()
            current_line = (
                current_line[:start] + replacement_function(match) + current_line[end:]
            )
            file_modified = True

        new_lines.append(current_line)

    if file_modified:
        if not dry_run:
            write_file(path, new_lines, encoding)
        else:
            print(f"[Dry-run] Would modify: {path}")
        return True

    return False


def main():
    args = parse_args()
    if args.help:
        print_help()
        sys.exit(0)

    search_regex = compile_search_pattern(args.search_pattern)
    matches = find_matching_files(
        args.no_ignore_files,
        args.file_pattern,
    )

    if not matches:
        if not args.quiet:
            print("No files found matching the pattern.")
        sys.exit(0)

    files_processed = 0
    files_modified = 0

    try:
        for path in matches:
            files_processed += 1
            if process_file(
                path,
                search_regex,
                args.replacement_pattern,
                ENCODING,
                args.interactive,
                args.dry_run,
            ):
                files_modified += 1
    except KeyboardInterrupt:
        sys.exit(0)

    if not args.quiet:
        print(f"\nProcessed {files_processed} files, modified {files_modified} files")


if __name__ == "__main__":
    main()
