# Misc scripts

## nextmeeting.py

display next calendar meeting in waybar or others nicely

![image](https://user-images.githubusercontent.com/98980/192647099-ccfa2002-0db3-4738-a54b-176a03474483.png)

## [gg](gg) - Grep with RG and FZF and edit it there

[![asciicast](https://asciinema.org/a/LbgR8IcB8iRMOpTTBVcccYaDK.svg)](https://asciinema.org/a/LbgR8IcB8iRMOpTTBVcccYaDK)

## [yjq](gg) - query yaml with jq

![yjq](.shots/yjq.png)

## [route53-delete-record](route53-delete-record.py)

How to delete route53 records with boto, cause it's an insane amount of `b****ck` with the aws cli
