#!/usr/bin/env bash
set -euo pipefail
TMPFILE=$(mktemp)
trap 'rm -f $TMPFILE' EXIT
opt_everything=
default_bind="--bind alt-n:next-history,alt-p:previous-history,ctrl-j:preview-down,ctrl-k:preview-up,ctrl-n:down,ctrl-p:up,ctrl-v:change-preview-window(right:wrap|down,70%:wrap)"

function help() {
  cat <<EOF
Usage: vig [options] [PATTERN]

-a  Show everything including hidden files
EOF
}

while getopts "ah" opt; do
  case $opt in
  a) opt_everything="--no-ignore --hidden" ;;
  h)
    help
    exit 0
    ;;
  *)
    echo "unknown option: -${OPTARG}" >&2
    help
    exit 1
    ;;
  esac
done
shift $((OPTIND - 1))

arg=
[[ -n $* ]] && arg="-q $*"
RG_PREFIX="rg --column --line-number --no-heading --color=always --smart-case ${opt_everything}"
export FZF_DEFAULT_COMMAND="$RG_PREFIX ''"
EDITOR=${EDITOR:-vim}
result=$(
  fzf --ansi --phony \
    --delimiter : \
    --bind "change:reload:$RG_PREFIX {q} || true" \
    --bind "start:reload:$RG_PREFIX {q} || true" \
    --bind "enter:execute(${EDITOR} {1} +{2})" \
    --preview-window=down:wrap \
    ${default_bind} \
    --preview "file-preview -v {} | rg --line-number --pretty --colors 'match:bg:yellow' --colors 'match:fg:black'  --ignore-case --context 3 {q}" \
    ${arg}
)
echo "$result"
