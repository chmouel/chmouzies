# argos plugins - https://github.com/rammie/argos/tree/gnome-3.36

Plugins that was available before on the internet, cleaned up ported to python3 adapted to work, YMMV if it works well for you

## GitHUB Notifications

<a href="https://ibb.co/1bRkdJG"><img src="https://i.ibb.co/52sPnxh/image.png" alt="image" border="0"></a>


## Google Calendar UP Next

<a href="https://ibb.co/0Gwrpyt"><img src="https://i.ibb.co/d5hDs4g/image-1.png" alt="image-1" border="0"></a>

## Emacs - Gnus Inbox

<img src="https://gist.githubusercontent.com/chmouel/c8a901ce7739d63854be3bf314bd596a/raw/f23a7d44235174ba56c87fd4db06b63dfd36e18e/screenshot.png">
