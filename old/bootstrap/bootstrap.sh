#!/usr/bin/env bash
# sudo apt-get -y install curl  && bash  <(curl http://www.chmouel.com/pub/bootstrap.sh)
BASE_PACKAGE="vim tmux git zsh fd-find ripgrep"
export DEB_PACKAGES="$BASE_PACKAGE ufw"
export RPM_PACKAGES="$BASE_PACKAGE"
export DEBIAN_FRONTEND=noninteractive
set -e

sudo sed -i '/^%\(wheel\|sudo\)/ { s/ALL$/NOPASSWD: ALL/ }' /etc/sudoers

# Don't use DNS for ssh
sudo sed -i -e 's/^#UseDNS/UseDNS/' -e "/UseDNS/ { s/no/yes/ ;}" /etc/ssh/sshd_config

type -p systemctl >/dev/null && sudo systemctl restart sshd

function debianies () {
    sudo apt-get -y install locales
    sudo locale-gen en_GB.UTF-8
    sudo dpkg-reconfigure locales
    sudo sed  -i '/^#\s*deb .*\(multiverse\|universe\)$/ { s/^#//; }' /etc/apt/sources.list
    sudo apt-get -y install ${DEB_PACKAGES}
    sudo update-alternatives --set editor /usr/bin/vim.basic

	sudo ufw allow proto tcp from any to any port 22 && \
		sudo ufw -f enable
}

function readties() {
    echo "assumeyes=True" | sudo tee -a /etc/dnf/dnf.conf >/dev/null

    sudo dnf -y update
    sudo dnf -y install ${RPM_PACKAGES}
}

# Install dev tools
if [[ -e /usr/bin/apt-get ]];then
    debianies
elif [[ -e /usr/bin/yum ]];then
    readties
fi


sudo usermod -s /bin/zsh $USER

mkdir -p $HOME/GIT/perso
cd $HOME/GIT/perso

for repo in rc zsh vim chmouzies;do
    [[ -d $repo-config ]] && continue
    git clone --recursive http://github.com/chmouel/${repo}-config.git
done

for f in git/gitconfig git/gitexclude tmux/screenrc tmux/tmux.conf;do
    b=$(basename ${f})
    ln -fs GIT/perso/rc-config/${f} ~/.${b}
done

ln -fs GIT/perso/zsh-config ~/.shell
ln -fs .shell/config/zshrc ~/.zshrc
ln -fs GIT/perso/vim-config ~/.vim

mkdir ${HOME}/bin
cd ${HOME}/bin
for i in ../GIT/perso/chmouzies/git/*;done
    [[ -x ${i} && -f ${i} ]] || continue
    ln -s ${i} .
done

vim +PluginInstall +qall || true

echo -e "#\n#hostColor=\"yellow\"\n#userColor=\"yellow\"\n" > ~/.shell/hosts/${HOSTNAME%%.*}.sh

if [[ -e /usr/bin/apt-get ]];then
   INST="apt-get"
elif [[ -e /usr/bin/dnf ]];then
   INST="dnf"
fi

if [[ -n ${INST} ]];then
    cat <<EOF>~/.shell/hosts/${HOSTNAME%%.*}.sh
# hostColor="yellow"
# userColor="white"

alias inst="sudo $INST -y install"
alias remove="sudo $INST -y remove"
alias g="git grep"

export LESS="-r"
EOF
fi
