#!/usr/bin/env bash
# Chmouel Boudjnah <chmouel@chmouel.com>
# You need to preconfigure acme with credentials and access etc.. see
# https://github.com/acmesh-official/acme.sh

set -eu

CLUSTER_NAME=${1:-os4}

[[ -z ${CLUSTER_NAME} ]] && exit 1

export CERTDIR=${HOME}/.acme.sh/certificates/${CLUSTER_NAME}
mkdir -p ${CERTDIR}

OC="oc"

export LE_API=$(${OC} whoami --show-server | cut -f 2 -d ':' | cut -f 3 -d '/' | sed 's/-api././')
export LE_WILDCARD=$(${OC} get ingresscontroller default -n openshift-ingress-operator -o jsonpath='{.status.domain}')
export LE_WORKING_DIR="${HOME}/.acme.sh"

function install_cert() {
    mkdir -p ${CERTDIR}
    $HOME/.acme.sh/acme.sh --issue -d ${LE_API} -d *.${LE_WILDCARD} --dns dns_aws
    ${HOME}/.acme.sh/acme.sh --install-cert -d ${LE_API} -d *.${LE_WILDCARD} --cert-file ${CERTDIR}/cert.pem --key-file ${CERTDIR}/key.pem --fullchain-file ${CERTDIR}/fullchain.pem --ca-file ${CERTDIR}/ca.cer
}

function configure_router_certs() {
    ${OC} delete secret router-certs -n openshift-ingress 2>/dev/null || true;
    ${OC} create secret tls router-certs --cert=${CERTDIR}/fullchain.pem --key=${CERTDIR}/key.pem -n openshift-ingress && \
    ${OC} patch ingresscontroller default -n openshift-ingress-operator --type=merge --patch='{"spec": { "defaultCertificate": { "name": "router-certs" }}}'
}

# Try to renew unless not needed
install_cert || true
configure_router_certs
