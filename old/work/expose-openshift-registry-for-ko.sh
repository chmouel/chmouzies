#!/usr/bin/env bash
# Chmouel Boudjnah <chmouel@chmouel.com>
set -xe

# Create a ~/.kube/htpass with :
#
# username:passwd
#
# (passwd in clear, make sure to chmod 0600 it)
#
# Credentials will be stored in ~/.docker/config.json the KO_DOCKER_REPO
# variable to use will be showed

# Set this up
target_ns=pipelines-as-code

cluster=$(oc whoami --show-server)

function os4_add_htpasswd_auth() {
	local htfile=/tmp/.kube.htpass.$$
    oc get secret htpasswd-secret -n openshift-config 2>/dev/null >/dev/null  && return || true
	htpasswd -b -c ${htfile} "$(head ~/.kube/htpass|awk -F: '{print $1}')" "$(head ~/.kube/htpass|awk -F: '{print $2}')"

    oc create secret generic htpasswd-secret \
       --from-file=htpasswd=$htfile -n openshift-config
    oc patch oauth cluster -n openshift-config --type merge --patch "spec:
  identityProviders:
  - htpasswd:
      fileData:
        name: htpasswd-secret
    mappingMethod: claim
    name: htpasswd
    type: HTPasswd
"
    oc adm policy add-cluster-role-to-user cluster-admin "$(head ~/.kube/htpass|awk -F: '{print $1}')"
	sleep 5
}

function expose_reg() {
	c=1
	while true;do
		oc login ${cluster} -u "$(head ~/.kube/htpass|awk -F: '{print $1}')" -p"$(head ~/.kube/htpass|awk -F: '{print $2}')" && break
		(( c++ ))
		[[ ${c} == 15 ]] && { echo "Timeout"; exit 1 ;}
		sleep 10
	done
	oc patch configs.imageregistry.operator.openshift.io/cluster --patch '{"spec":{"defaultRoute":true}}' --type=merge
	sleep 5
	oc registry login --to=$HOME/.docker/config.json --registry=$(oc get route default-route -n openshift-image-registry --template='{{ .spec.host }}')
}

function createns() {
	oc get ns ${target_ns} 2>/dev/null >/dev/null && return
	oc new-project ${target_ns}
}

os4_add_htpasswd_auth
expose_reg
createns

echo "export KO_DOCKER_REPO=$(oc get route default-route -n openshift-image-registry --template='{{ .spec.host }}')/${target_ns}"
