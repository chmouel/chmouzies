#!/usr/bin/env bash
# Copyright 2023 Chmouel Boudjnah <chmouel@chmouel.com>
set -eufo pipefail
BASEDIR=$GOPATH/src/github.com/openshift-pipelines/pac
WORKTREE=main
E2E_RUN_LAST=$HOME/.cache/pm/last

if ! pgrep -f "^(/.*/)?docker\b" >/dev/null; then
	if type -p systemctl &>/dev/null; then
		sudo systemctl start docker
		sleep 2
	else
		echo "need docker running"
	fi
fi

function read_last() {
	if [[ -e $E2E_RUN_LAST ]]; then
		read -a args <<<"$(cat ${E2E_RUN_LAST})"
		WORKTREE=${args[0]}
		unset "args[0]"
	fi
}

args=()
if [[ ${#@} -gt 0 ]]; then
	read -a args <<<"$*"
	lastargument=${args[-1]}
	if [[ -n ${lastargument} && -e ${BASEDIR}/${lastargument} ]]; then
		WORKTREE=$lastargument
		unset "args[-1]"
	elif [[ $lastargument != -* && -e $E2E_RUN_LAST ]]; then
		read_last
	fi
elif [[ -e $E2E_RUN_LAST ]]; then
	read_last
fi

echo "Installing from ${WORKTREE} worktree"
args="${args[@]:-}"
${BASEDIR}/${WORKTREE}/hack/dev/kind/install.sh "${args[@]}"

[[ " ${args[*]} " =~ " -b " ]] && {
	echo -e "\e[32;1m"
	echo "🚀 Kind has been installed successfully!"
	echo -ne "\e[0m"
	exit 0
}

export KUBECONFIG=$HOME/.kube/config.kind
kubectl explain repositories.pipelinesascode.tekton.dev >/dev/null 2>/dev/null || {
	echo "PaaC was not installed properly. exiting"
	exit 1
}

# Deploy a second controller if it exist
[[ -e $HOME/Sync/paac/random/second-controller/second-controller.sh ]] && {
	$HOME/Sync/paac/random/second-controller/second-controller.sh
}

# removing gosmee doublons
kubectl delete deployment -n gitea gosmee 2>/dev/null || true

if type -p systemctl &>/dev/null && [[ -e $HOME/.config/systemd/user/gosmee.service ]]; then
	echo "Starting Gosmee service"
	systemctl --user restart gosmee 2>/dev/null || true
fi

if type -p launchctl &>/dev/null && [[ -e $HOME/Library/LaunchAgents/com.chmouel.gosmee.plist ]]; then
	echo "Starting Gosmee service"
	launchctl unload -w $HOME/Library/LaunchAgents/com.chmouel.gosmee.plist 2>/dev/null || true
	launchctl load -w $HOME/Library/LaunchAgents/com.chmouel.gosmee.plist 2>/dev/null || true
fi

[[ -d $HOME/Sync/paac/crds ]] && {
	echo "Installing custom CRDS"
	kubectl apply -f $HOME/Sync/paac/crds
}
