#!/usr/bin/env bash
# Copyright 2022 Chmouel Boudjnah <chmouel@chmouel.com>
# use gh to  list pr and open it
set -eufo pipefail
initial_query=()
createbranch=yes
checkstatus=
nothing=
state=open
limit=50
default_fzf_bind=(
  --preview-window "up,50%:wrap"
  --bind "alt-n:next-history,alt-p:previous-history,ctrl-j:preview-down,ctrl-k:preview-up,ctrl-n:down,ctrl-p:up,ctrl-v:change-preview-window(right:wrap|up,50%:wrap)")

TMP=$(mktemp /tmp/.mm.XXXXXX)
clean() { rm -f ${TMP}; }
trap clean EXIT

function help() {
  cat <<EOF
  Usage: gh-prcheck [-vcn] [query]

  -n: nothing just browse
  -v: view on github directly
  -c: check ci status on each PR by colorizing it
EOF
}

while getopts "xznchvL:" o; do
  case "${o}" in
  x)
    state=closed
    ;;
  n)
    nothing=yes
    ;;
  L) limit=${OPTARG} ;;
  c)
    checkstatus=yes
    ;;
  v)
    createbranch=
    ;;
  h)
    help
    exit 0
    ;;
  *)
    echo "Invalid option"
    help
    exit 1
    ;;
  esac
done
shift $((OPTIND - 1))

git rev-parse --show-toplevel >/dev/null || exit 1

colorize() {
  while read -r line; do
    if [[ $line =~ (#[0-9]+)(.*) ]]; then
      pr=${BASH_REMATCH[1]}
      rest=${BASH_REMATCH[2]}
      checks=$(GH_FORCE_TTY='100%' gh pr checks "$pr" || true)
      if [[ $checks =~ ([0-9]+)\ cancelled,\ ([0-9]+)\ failing,\ ([0-9]+)\ successful,\ ([0-9]+)\ skipped,\ and\ ([0-9]+)\ pending ]]; then
        cancelled=${BASH_REMATCH[1]}
        failing=${BASH_REMATCH[2]}
        successful=${BASH_REMATCH[3]}
        skipped=${BASH_REMATCH[4]}
        pending=${BASH_REMATCH[5]}
        if [[ $cancelled -gt 0 || $skipped -gt 0 ]]; then
          color=221
        elif [[ $failing -gt 0 ]]; then
          color=196
        elif [[ $pending -gt 0 ]]; then
          color=179
        elif [[ $successful -gt 0 ]]; then
          color=156
        fi
        # using 256 colors replace pr with the color
        prcolor=$(
          echo -e "\033[38;5;${color}m${pr}\033[0m"
        )
        echo $(tput setaf 214)$prcolor $rest >>${TMP}
      fi
    fi
  done <<<$list
}

[[ -n $* ]] && initial_query=(-q "$@")
IFS=$'\n'

if [[ -n $checkstatus ]]; then
  list=$(GH_FORCE_TTY='100%' gh pr list --state=${state} -L "${limit}")
  echo $list | colorize
  exit
fi
# shellcheck disable=SC2016
IFS=$'\n' read -r -a prs < <(
  GH_FORCE_TTY='100%' gh pr list --state=${state} -L "${limit}" | fzf \
    "${default_fzf_bind[@]}" \
    "${initial_query[@]}" --reverse \
    --header-lines=4 --header-first \
    --nth=1 -1 -0 -m --ansi \
    --preview 'GH_FORCE_TTY=$FZF_PREVIEW_COLUMNS gh pr view {1}'
)

[[ -z ${prs[0]} ]] && exit
for pr in "${prs[@]}"; do
  p=${pr##\#}
  p=${p%% *}
  if [[ -n ${nothing} ]]; then
    echo $p
  elif [[ -n ${createbranch} ]]; then
    gh pr checkout ${p}
  else
    gh pr view -w ${p}
  fi
done
