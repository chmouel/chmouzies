#!/usr/bin/env bash
# Copyright 2023 Chmouel Boudjnah <chmouel@chmouel.com>
set -eufo pipefail
MAX_BRANCH_NAME_LENGTH=50

function relinks() {
  [[ -d ../main ]] || exit 0
  for i in $(fd -H --no-ignore-vcs -tl -d1 . ../main/); do ln -sf $i .; done
  ln -sf ../main/.ignore ./
  direnv allow
}

[[ ${1-:""} == -r ]] && {
  relinks
  exit 0
}

newbranch=${1-""}
chosen=${2-""}

if [[ -z "$chosen" ]]; then
  chosen=$(
    git branch --sort=-committerdate --format='%(refname:short)' |
      fzf --border --preview "git log --no-merges --patch-with-stat -1 {}|bat --language=diff --color=always" \
        --preview-window=up:60% --prompt="Choose Branch to start from> " --height=100% --ansi --no-sort --tiebreak=index
  )
fi

if [[ -z "$newbranch" ]]; then
  # ask for a branch name to create
  newbranch=$(gum input --width=${MAX_BRANCH_NAME_LENGTH} --value "$chosen" --header "New branch name")
  [[ -z "$newbranch" ]] && {
    echo "No branch name given, aborting" >&2
    exit 1
  }
fi

git worktree add -q -B "$newbranch" "../$newbranch" "$chosen"
cd "../$newbranch"
relinks
echo $(readlink -f .)
