#!/usr/bin/env bash
set -eufo pipefail
chosen=${1:-""}
if [[ -z ${chosen} ]]; then
	if [[ -e $HOME/.config/zsh/functions/fzf-git.source ]]; then
		chosen=$(bash $HOME/.config/zsh/functions/fzf-git.source branches |
			fzf --ansi --tac --prompt '🌲 Branches> ' --header-lines 2 --tiebreak begin \
				--preview-window down,border-top,40% \
				--bind 'ctrl-/:change-preview-window(down,70%|hidden|)' \
				--preview 'git log --oneline --graph --date=short --color=always --pretty="format:%C(auto)%cd %h%d %s" $(sed s/^..// <<< {} | cut -d" " -f1)' \
				--color hl:underline,hl+:underline | sed 's/^..\([^ ]*\) (.*/\1/')
	else
		chosen=$(
			git branch --sort=-committerdate --format='%(refname:short)' |
				fzf --border --preview "git log --no-merges --patch-with-stat -1 {}|bat --language=diff --color=always" \
					--preview-window=up:80%
		)
	fi
fi

[[ ${1-""} == -n ]] && {
	echo $chosen
	exit 0
}
git checkout ${chosen}
