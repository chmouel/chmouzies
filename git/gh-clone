#!/usr/bin/env bash
# Chmouel Boudjnah <chmouel@chmouel.com>
set -eufo pipefail

fork=
single=
current=
userremote=${USER:-chmouel}
add_fork_remote=
gitlab=
fork_repo_name=
ssh=
server=github.com

function help() {
  cat <<EOF
-f fork
-1 single depth
-c checkout in current directory
-a add remote '${userremote}' with the fork
-A the repository name of the forked remote, default same as the upstream repo
-p private repository checkout from a private repok
-l Use gitlab
EOF
}

while getopts "phfc1laA:" o; do
  case "${o}" in
  f) fork=true ;;
  a) add_fork_remote=true ;;
  A) fork_repo_name=${OPTARG} ;;
  1) single="--depth=1" ;;
  l) server=gitlab.com ;;
  c) current=true ;;
  h)
    help
    exit
    ;;
  p) ssh=true ;;
  *)
    echo "Invalid option"
    help
    exit 1
    ;;
  esac
done
shift $((OPTIND - 1))

repo=${1:-""}
if [[ ${repo} == https://github.com* ]]; then
  repo=${repo/https:\/\/github.com\//}
elif [[ ${repo} == gitlab.com* || ${repo} == https://gitlab.com* ]]; then
  server=gitlab.com
  repo=${repo/https:\/\/gitlab.com\//}
fi

[[ -z ${repo} ]] && {
  echo "no repo specificed"
  exit 1
}

shift

gitargs=("${@}")
org=$(echo ${repo} | awk -F/ '{print $1}')
repo=$(echo ${repo} | awk -F/ '{print $2}')
[[ ${current} ]] && bdir="." || bdir=$GOPATH/src/${server}/${org}
url=https://${server}/${org}/${repo}
remotebranch=origin

if [[ ${org} == "${userremote}" || -n ${ssh} ]]; then
  url=git@${server}:$org/$repo
fi

if [[ ! -d ${bdir}/${repo} ]]; then
  [[ -d $bdir ]] || mkdir -p ${bdir}
  cd ${bdir}
  git clone ${single} "${gitargs[@]}" ${url}
fi

fpath=$(readlink -f ${bdir}/${repo})
cd ${fpath}

if [[ -n ${fork} && ${org} != "${userremote}" ]]; then
  if ! git remote get-url "${userremote}" >/dev/null 2>/dev/null; then
    cd ${bdir}/${repo}
    gh repo fork || true
    git remote set-url ${remotebranch} $(git remote get-url ${remotebranch} | sed "s,https://${server}/,git@${server}:,")
  fi
elif [[ -n ${add_fork_remote} && ${org} != "${userremote}" ]]; then
  cd ${bdir}/${repo}
  fork_repo_name=${fork_repo_name:-${repo}}
  git remote add "${userremote}" git@${server}:chmouel/${fork_repo_name}.git
  git fetch -a "${userremote}"
elif [[ ${org} == "${userremote}" ]]; then
  git remote set-url origin $(git remote get-url origin | sed "s,https://${server}/,git@${server}:,")
fi

echo ${fpath}

c=
type -p pbcopy 2>/dev/null >/dev/null && c=pbcopy
[[ -n ${c} ]] && echo ${fpath} | ${c}
