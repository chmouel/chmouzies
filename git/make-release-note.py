#!/usr/bin/env python3
import argparse
import os
import re
import subprocess
import sys
from datetime import datetime

import requests


def get_repo_from_remote():
    """Extract repository owner and name from git remote origin URL"""
    try:
        result = subprocess.run(
            ["git", "remote", "get-url", "origin"],
            capture_output=True,
            text=True,
            check=True,
        )
        remote_url = result.stdout.strip()

        # Handle SSH URLs like: git@github.com:owner/repo.git
        if remote_url.startswith("git@"):
            path = remote_url.split(":")[1]
        # Handle HTTPS URLs like: https://github.com/owner/repo.git
        else:
            path = remote_url.split("github.com/")[-1]

        # Remove .git suffix if present and get last two components
        path = path.replace(".git", "")
        components = path.strip("/").split("/")
        if len(components) < 2:
            raise Exception("Could not parse owner/repo from remote URL")

        return f"{components[-2]}/{components[-1]}"

    except subprocess.CalledProcessError:
        raise Exception(
            "Failed to get git remote URL. Make sure you're in a git repository."
        )


def get_current_git_tag():
    """Get the current git tag from command line"""
    try:
        result = subprocess.run(
            ["git", "describe", "--tags", "--abbrev=0"],
            capture_output=True,
            text=True,
            check=True,
        )
        return result.stdout.strip()
    except subprocess.CalledProcessError:
        raise Exception(
            "Failed to get current git tag. Make sure you're in a git repository with tags."
        )


def get_previous_minor_version(current_tag):
    """Calculate the previous minor version from current tag"""
    # Remove 'v' prefix if it exists
    version = current_tag.lstrip("v")

    # Parse the semantic version
    match = re.match(r"(\d+)\.(\d+)\.(\d+)", version)
    if not match:
        raise Exception(f"Invalid semantic version format: {current_tag}")

    major, minor, patch = map(int, match.groups())

    # Decrease minor version
    if minor > 0:
        minor -= 1
        patch = 0
    else:
        # If minor is 0, decrease major version if possible
        if major > 0:
            major -= 1
            minor = 0
            patch = 0
        else:
            raise Exception("Cannot calculate previous version: already at 0.0.0")

    # Reconstruct the version string with 'v' prefix if it was present
    prefix = "v" if current_tag.startswith("v") else ""
    return f"{prefix}{major}.{minor}.{patch}"


def get_tag_date(repo, tag, token):
    """Get the creation date of a tag, handling both lightweight and annotated tags"""
    headers = {
        "Authorization": f"token {token}",
        "Accept": "application/vnd.github.v3+json",
    }

    # First get the tag reference
    url = f"https://api.github.com/repos/{repo}/git/refs/tags/{tag}"
    response = requests.get(url, headers=headers)
    if response.status_code != 200:
        raise Exception(f"Failed to get tag {tag}: {response.status_code}")

    tag_data = response.json()
    object_type = tag_data["object"]["type"]
    object_sha = tag_data["object"]["sha"]

    if object_type == "tag":
        # This is an annotated tag
        url = f"https://api.github.com/repos/{repo}/git/tags/{object_sha}"
        response = requests.get(url, headers=headers)
        if response.status_code != 200:
            raise Exception(f"Failed to get annotated tag: {response.status_code}")
        tagger_info = response.json().get("tagger", {})
        if tagger_info and "date" in tagger_info:
            return datetime.strptime(tagger_info["date"], "%Y-%m-%dT%H:%M:%SZ")

    # For lightweight tags or if we couldn't get the tag date
    # Get the commit date instead
    url = f"https://api.github.com/repos/{repo}/git/commits/{object_sha}"
    response = requests.get(url, headers=headers)
    if response.status_code != 200:
        raise Exception(f"Failed to get commit: {response.status_code}")

    commit_data = response.json()
    commit_date = commit_data["committer"]["date"]
    return datetime.strptime(commit_date, "%Y-%m-%dT%H:%M:%SZ")


def format_markdown_release_notes(prs, start_tag, end_tag):
    """Format PRs as markdown release notes"""
    if not prs:
        return "No pull requests found for this period."

    content = f"# Release Notes: {start_tag} → {end_tag}\n\n"

    # Group PRs by labels
    labeled_prs = {}
    unlabeled_prs = []

    for pr in prs:
        if pr["labels"]:
            for label in pr["labels"]:
                if label not in labeled_prs:
                    labeled_prs[label] = []
                labeled_prs[label].append(pr)
        else:
            unlabeled_prs.append(pr)

    # Format grouped PRs
    for label, prs_list in labeled_prs.items():
        content += f"\n## {label}\n\n"
        for pr in prs_list:
            content += f"### {pr['title']} (#{pr['number']})\n\n"
            content += f"**Author:** {pr['author']}\n"
            content += f"**Merged:** {pr['merged_at']}\n"
            content += f"**URL:** {pr['url']}\n\n"
            content += f"{pr['description']}\n\n"
            content += "---\n\n"

    # Add unlabeled PRs if any
    if unlabeled_prs:
        content += "\n## Other Changes\n\n"
        for pr in unlabeled_prs:
            content += f"### {pr['title']} (#{pr['number']})\n\n"
            content += f"**Author:** {pr['author']}\n"
            content += f"**Merged:** {pr['merged_at']}\n"
            content += f"**URL:** {pr['url']}\n\n"
            content += f"{pr['description']}\n\n"
            content += "---\n\n"

    return content


def get_prs_between_tags(repo, start_tag, end_tag, token):
    """Get all PRs merged between two tags"""
    try:
        start_date = get_tag_date(repo, start_tag, token)
        end_date = get_tag_date(repo, end_tag, token)

        # Ensure correct chronological order
        if start_date > end_date:
            start_date, end_date = end_date, start_date
            start_tag, end_tag = end_tag, start_tag

        headers = {
            "Authorization": f"token {token}",
            "Accept": "application/vnd.github.v3+json",
        }

        url = f"https://api.github.com/repos/{repo}/pulls"
        params = {
            "state": "closed",
            "sort": "updated",
            "direction": "desc",
            "per_page": 100,
        }

        all_prs = []
        page = 1

        while True:
            params["page"] = page
            response = requests.get(url, headers=headers, params=params)
            if response.status_code != 200:
                raise Exception(f"Failed to get PRs: {response.status_code}")

            prs = response.json()
            if not prs:
                break

            for pr in prs:
                if pr["merged_at"] is None:
                    continue

                merged_date = datetime.strptime(pr["merged_at"], "%Y-%m-%dT%H:%M:%SZ")
                if start_date <= merged_date <= end_date:
                    # Clean up description text
                    description = (
                        pr.get("body", "").strip() or "No description provided"
                    )
                    # Remove any markdown horizontal rules that might interfere with our formatting
                    description = re.sub(
                        r"^\s*[-_*]{3,}\s*$", "", description, flags=re.MULTILINE
                    )

                    all_prs.append(
                        {
                            "number": pr["number"],
                            "title": pr["title"],
                            "description": description,
                            "merged_at": pr["merged_at"],
                            "url": pr["html_url"],
                            "author": pr["user"]["login"],
                            "labels": [label["name"] for label in pr["labels"]],
                        }
                    )
                elif merged_date < start_date:
                    return all_prs

            page += 1

        return all_prs

    except Exception as e:
        print(f"Error: {str(e)}")
        return None


def main():
    parser = argparse.ArgumentParser(
        description="Generate release notes between two git tags."
    )
    parser.add_argument("old_release", nargs="?", help="The old release tag.")
    parser.add_argument("current_release", nargs="?", help="The current release tag.")
    parser.add_argument("--github-token", help="Your GitHub personal access token.")
    parser.add_argument(
        "--pass-key", help="The key to retrieve the GitHub token from pass."
    )

    args = parser.parse_args()

    if args.github_token:
        token = args.github_token
    else:
        token = os.environ.get("GITHUB_TOKEN")
        if not token and args.pass_key:
            try:
                token = subprocess.run(
                    ["pass", "show", args.pass_key],
                    capture_output=True,
                    text=True,
                    check=True,
                ).stdout.strip()
            except subprocess.CalledProcessError:
                print(
                    f"Error: Failed to retrieve token using pass key: {args.pass_key}"
                )
                sys.exit(1)
        if not token:
            print(
                "Error: GitHub token is required. "
                "Provide it via --github-token, GITHUB_TOKEN environment variable, or --pass-key."
            )
            sys.exit(1)

    try:
        repo = get_repo_from_remote()
        print(f"Using repository from git remote: {repo}")

        if args.old_release and args.current_release:
            old_release = args.old_release
            current_release = args.current_release
        else:
            current_release = get_current_git_tag()
            old_release = get_previous_minor_version(current_release)

        print(f"Comparing changes between {old_release} and {current_release}")

        prs = get_prs_between_tags(repo, old_release, current_release, token)

        if prs:
            # Format as markdown and write to file
            markdown_content = format_markdown_release_notes(
                prs, old_release, current_release
            )
            print(markdown_content)
        else:
            print("No pull requests found or an error occurred.")

    except Exception as e:
        print(f"Error: {str(e)}")
        sys.exit(1)


if __name__ == "__main__":
    main()
