#compdef gh-issuecreate
local ret=1
local -a state state_descr args

__gh_repos() {
    gh api graphql -q ".data[].repositories[].[] | (.name + \":\" + .description )" \
        --paginate -f owner=${1-${USER}} -f query='query Repos($owner: String!, $endCursor: String) {
	repositoryOwner(login: $owner) {
		repositories(
			first: 100
			ownerAffiliations: OWNER
			privacy: PUBLIC
			isFork: false
			isLocked: false
			orderBy: { field: PUSHED_AT, direction: DESC }
			after: $endCursor
		) {
			nodes {
				name
				description
			}
		}
	}
}'
}

__gh_repos_author() {
    local owner=${1/\/*} repo=${1/*\//}
    local Q='query Repos($owner: String!, $repo: String!) {
  repository(owner: $owner, name: $repo) {
    id
    name

    collaborators(first: 10, affiliation: ALL) {
      edges {
        permission
        node {
          login
          name
        }
      }
    }
  }
}
'
    gh api graphql -q ".data[].collaborators[][].node | (.login + \":\" + .name )" \
        -f owner=${owner} -f repo=${repo} -f query=${Q} 2>/dev/null
}

__gh_repos_labels() {
    local owner=${1/\/*} repo=${1/*\//}
    gh api --jq '.[] | ( .name + ":" + .description)' /repos/${owner}/${repo}/labels
}

__gh_repos_milestones() {
    local owner=${1/\/*} repo=${1/*\//}
    gh api --jq '.[] | ( .title)' /repos/${owner}/${repo}/milestones
}

_arguments -s -S -C \
        \*{-a,--assignee}'[Assign people by their login. Use '@me" to self-assign.]"': :->author' \
        \*{-l,--label}"[set label]"': :->label' \
        \*{-m,--milestone}"[set milestone]"': :->milestone' \
        '-e[edit]' \
        "1:git-org-repositories:->org-repositories"


case $state in
    (milestone)
        local oRepoi=${words[(I)*/*]}
        if (( oRepoi )) then
            local -a args=("${(@f)$(__gh_repos_milestones ${words[$oRepoi]})}")
            _describe -t gh-milestones arg args && ret=0
        fi
        ;;
    (label)
        local oRepoi=${words[(I)*/*]}
        if (( oRepoi )) then
            local -a args=("${(@f)$(__gh_repos_labels ${words[$oRepoi]})}")
            (( ! ${args[(I)*FORBIDDEN*]} )) && [[ ! ${args} == \{* ]] && {
                _describe -t gh-labels arg args && ret=0
            }
        fi
        ;;
    (author)
        local oRepoi=${words[(I)*/*]}
        if (( oRepoi )) then
            local -a args=("${(@f)$(__gh_repos_author ${words[$oRepoi]})}")
            (( ! ${args[(I)*FORBIDDEN*]} )) && [[ ! ${args} == \{* ]] && {
                _describe -t gh-authors arg args && ret=0
            }
            set +x
        fi
        ;;
    (org-repositories)
        if compset -P '(#b)(*)/'; then
            local -a args=("${(@f)$(__gh_repos ${match[@]})}")
            _describe -t git-repositories arg args && ret=0
        elif (( $+GH_ISSUECREATE_ORGS )); then
            compadd -X "GitHub Organization" -S /  ${(z)GH_ISSUECREATE_ORGS} && ret=0
        fi
        ;;
esac

return ret

# Local Variables:
# mode: shell-script
# End:
