#!/usr/bin/env bash
# Copyright 2022 Chmouel Boudjnah <chmouel@chmouel.com>
# need to be installed as an exectuable called `gh` in your PATH before where the real gh binary
# is located to work.
# i.e: add it to ~/.local/bin and set your path like this export PATH=$HOME/.local/bin:$PATH in your shell
set -eufo pipefail

[[ -n ${GH_COMPLETER_DEBUG_FILE:-""} ]] && {
	set -x
	exec 19>${GH_COMPLETER_DEBUG_FILE}
	BASH_XTRACEFD=19
	echo "'$*'" >>$GH_COMPLETER_DEBUG_FILE
}

## TODO: better place to define this"
declare -A extra_plugins=(
	["issuef"]="Open an issue with FZF"
	["clone"]="Clone a GitHub repo in \$GOPATH"
	["prcheck"]="Checkout a PullRequest with FZF"
	["run-logview"]="Show run logview"
)

ghbin=""
mapfile -t ghbins < <(command which -a gh)
ghbins+=(/opt/homebrew/bin/gh)
for i in "${!ghbins[@]}"; do
	if [[ ${ghbins[$i]} == /home/* || ${ghbins[$i]} == /Users/* ]]; then
		continue
	fi
	[[ ! -e ${ghbins[$i]} ]] && {
		continue
	}
	ghbin=${ghbins[$i]}
done

[[ -z ${ghbin} ]] && {
	echo "cannot find real gh bin in path"
	exit 1
}

TMP=$(mktemp /tmp/.mm.XXXXXX)
clean() { rm -f ${TMP}; }
trap clean EXIT

get_obj_title() {
	local obj=$1
	${ghbin} $obj list -q '.[] | ((.number|tostring)+"\t"+.title)' --json "number,title" >${TMP}
	[[ -s ${TMP} ]] && cat ${TMP} || echo ":0"
}

if [[ "$*" =~ __complete\ pr\ (checks|close|comment|ready|review|status|view|diff|edit|merge|view|checkout)\ ($|([0-9]*)$) ]]; then
	get_obj_title pr
	exit
elif [[ "$*" =~ __complete\ issue\ (comment|view|delete|develop|edit|list|pin|status)\ ($|([0-9]*)$) ]]; then
	get_obj_title issue
elif [[ ${1:-""} == __complete && -z ${2:-""} ]]; then
	${ghbin} __complete '' | sed 's/^:[0-9]*//'
	for i in "${!extra_plugins[@]}"; do
		type -p gh-$i >/dev/null 2>/dev/null || continue
		echo -e "${i}\t${extra_plugins[$i]}"
	done
	cat <<EOF
:4
EOF
else
	if [[ -n ${1:-""} && -n ${extra_plugins[$1]-""} ]]; then
		IFS=" " read -r -a args <<<"$@"
		exec gh-${args[0]} "${args[@]:1}"
	else
		exec ${ghbin} "$@"
	fi
fi
